#include <iostream>
#include <list>
using namespace std;
#include "Proteina.h"
#include "Cadena.h"

// Constructor
Proteina::Proteina(string nombre, string id){
    this->nombre = nombre;
    this->id = id;
    list <string> cadenas;
}

// Metodos get and set
void Proteina::set_nombre(string nombre){
    this->nombre = nombre;
}

void Proteina::set_id(string id){
    this->id = id;
}

string Proteina::get_nombre(){
    return this->nombre;
}

string Proteina::get_id(){
    return this->id;
}
// Se agrega las cadena a la proteina
void Proteina::add_cadena(Cadena cadena){
    this->cadenas.push_back(cadena);
}

list<Cadena> Proteina::get_cadena(){
    return this->cadenas;
}
