#include <iostream>
#include <list>
#include "Aminoacido.h"
using namespace std;

#ifndef CADENA_H
#define CADENA_H

class Cadena {
    private:
        string letra;
        list<Aminoacido> aminoacidos;
    public:
        Cadena(string letra);

        //metodo set and get
        void set_letra(string letra);
        string get_letra();
        void add_aminoacido(Aminoacido aminoacido);
        list<Aminoacido> get_aminoacidos();
};
#endif
