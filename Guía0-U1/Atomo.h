#include <iostream>
using namespace std;
#include "Coordenada.h"

#ifndef ATOMO_H
#define ATOMO_H

class Atomo {
    private:
        string nombre_atomo = "\0";
        int numero_atomo = 0;
        Coordenada coordenada;
    public:
        Atomo(string nombre, int numero);

        // metodos get y set
        void set_nombre_atomo(string nombre);
        void set_numero_atomo(int numero);
        string get_nombre_atomo();
        int get_numero_atomo();

        void set_coordenada(Coordenada coordenda);
        Coordenada get_coordenada();

};
#endif
