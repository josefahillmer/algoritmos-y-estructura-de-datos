#include <iostream>
#include <list>
using namespace std;
#include "Aminoacido.h"
#include "Atomo.h"

Aminoacido::Aminoacido(string nombre, int numero){
    this->nombre_aa = nombre;
    this->numero_aa = numero;
    list <string> Atomo;
}

// Metodos set and get

void Aminoacido::set_nombre_aminoacido(string nombre){
    this->nombre_aa = nombre;
}

void Aminoacido::set_numero_aminoacido(int numero){
    this->numero_aa = numero;
}

string Aminoacido::get_nombre_aminoacido(){
    return this->nombre_aa;
}

int Aminoacido::get_numero_aminoacido(){
    return this->numero_aa;
}

void Aminoacido::add_atomo(Atomo atomo){
  this->atomos.push_back(atomo);

}

list<Atomo> Aminoacido::get_atomos(){
    return this->atomos;
}
