#include <iostream>
#include <list>
#include "Cadena.h"
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
    private:
        string nombre = "\0";
        string id = "\0";
        list <Cadena> cadenas;
    public:
        Proteina(string nombre, string id);
        // metodos set y get
        void set_id(string id);
        void set_nombre(string nombre);
        void add_cadena(Cadena cadena);
        string get_nombre();
        string get_id();
        list <Cadena> get_cadena();
};
#endif
