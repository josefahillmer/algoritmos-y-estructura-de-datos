
#include <list>
#include <iostream>
using namespace std;
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"


// Funcion menu
int menu(){
  string op;
  cout<<"\n\t\tLISTA DE PROTEINAS\n\n";
  cout<<" 1. Ingresar proteinas                 "<<endl;
  cout<<" 2. Ver lista de proteínas ingresadas  "<<endl;
  cout<<" 3. Proteinas en el programa           "<<endl;
  cout<<" 4. Salir                              "<<endl;
  cout<<"\n Ingrese opcion: ";
  getline(cin, op);
  return stoi(op);
}

//Funcion que imprime las proteinas
void imprimir_datos_proteina(list<Proteina> proteinas){
  cout<<"\n LISTA DE PROTEINAS \n";
    for(Proteina p: proteinas){
        cout<<"\n-------------------------------------------------------------\n";
        cout << "Nombre proteina: " << p.get_nombre() << endl;
        cout << "ID: " << p.get_id() << endl;
        for(Cadena c: p.get_cadena()){
            cout << "Cadena: " << c.get_letra() << endl;
            for(Aminoacido a: c.get_aminoacidos()){
                cout << "Numero de aminoacidos: " << a.get_numero_aminoacido() << endl;
                cout << "Nombre de aminoacido: " << a.get_nombre_aminoacido() << endl;
                for(Atomo at: a.get_atomos()){
                    cout << "Numero de atomos: " << at.get_numero_atomo() << endl;
                    cout << "Nombre de atomo: " << at.get_nombre_atomo() << endl;
                    cout << "Coordenadas: \n";
                    cout << "x: " << at.get_coordenada().get_x() << endl;
                    cout << "y: " << at.get_coordenada().get_y() << endl;
                    cout << "z: " << at.get_coordenada().get_z() << endl;
                }
            }
        }
    }
}


list<Proteina> leer_datos_proteinas(list<Proteina> proteinas){
  // Proteina en el sistema
  Proteina p1 = Proteina("Hemoglobina", "2GTL");
  Cadena p1_c = Cadena("A");
  Aminoacido p1_aa = Aminoacido("MET", 5);
  Atomo p1_at = Atomo("H", 1);
  p1_at.set_coordenada(Coordenada (9.546, 36.542, -15.58));
  p1_aa.add_atomo(p1_at);
  p1_c.add_aminoacido(p1_aa);
  p1.add_cadena(p1_c);
  proteinas.push_back(p1);
  imprimir_datos_proteina(proteinas);

  return proteinas;
}

// Funcion que pide ingresar los datos de la proteina al usuario
list<Proteina> ingresar_datos(list<Proteina> proteinas){
  //variables
  string nombre;
  string id;
  string letra;
  string aminoacidos;
  string aa;
  string atomos;
  string nom_atom;
  string x, y, z;

  cout << "Ingrese el nombre de su proteina: ";
  getline(cin, nombre);
  cout << "Ingrese ID: ";
  getline(cin, id);
  // Se crea la lista que tendra la proteina ingresada por el usuario
  Proteina proteina = Proteina(nombre, id);
  cout << "Ingrese la letra de la cadena: ";
  getline(cin, letra);
  // Se crea la lista de cadena
  Cadena cadena = Cadena(letra);
  cout << "Ingrese el numero de aminoacidos: ";
  getline(cin, aminoacidos);
  //Ciclo que agrega la cantidad de aminoacido que tiene la proteina
  for(int j=0; j<stoi(aminoacidos); j++){
      cout << "Ingrese el nombre del aminoacido " << j+1 << ": ";
      getline(cin, aa);
      Aminoacido aminoacido = Aminoacido(aa, j+1);
      cout << "Ingrese el numero de atomos: ";
      getline(cin, atomos);
      //Ciclo que agrega la cantidad de atomos que tiene la proteina
      for(int h=0; h<stoi(atomos); h++){
          cout << "Ingrese nombre del atomo " << h+1 << ": ";
          getline(cin, nom_atom);
          Atomo atomo = Atomo(nom_atom, h+1);
          cout << "Ingrese las coordenadas" << endl;
          cout << "x: ";
          getline(cin, x);
          cout << "y: ";
          getline(cin, y);
          cout << "z: ";
          getline(cin, z);
          // Se guardan las coordenadas y aminoacidos
          // stof convertira el string en float
          atomo.set_coordenada(Coordenada(stof(x), stof(y), stof(z)));
          aminoacido.add_atomo(atomo);
      }
      // Se agregan aminoacidos
      cadena.add_aminoacido(aminoacido);
  }
  // Se agregan las cadenas
  proteina.add_cadena(cadena);
  // Se agrega todo a la lista de proteinas
  proteinas.push_back(proteina);

  return proteinas;
}

// Funcion principal
int main(){
  int op;
  // Lista donde se gurdaran las proteinas
  list<Proteina> proteinas;

  do{
        op = menu();
        switch (op) {
          case 1:
                proteinas = ingresar_datos(proteinas);
        break;

          case 2:
                imprimir_datos_proteina(proteinas);
        break;

          case 3:
                proteinas = leer_datos_proteinas(proteinas);


        }
    cout<<endl<<endl;

  }while(op!=4);
  cout << "Byee" << endl;
  system("pause");
  return 0;
}