#include <iostream>
using namespace std;
#include "Atomo.h"
#include "Coordenada.h"

// Constructor
Atomo::Atomo(string nombre, int numero){
    this->nombre_atomo = nombre;
    this->numero_atomo = numero;
    this->coordenada = Coordenada();
}

// Metodos set and get
void Atomo::set_nombre_atomo(string nombre){
    this->nombre_atomo = nombre;
}

void Atomo::set_numero_atomo(int numero){
    this-> numero_atomo = numero;
}

string Atomo::get_nombre_atomo(){
    return this->nombre_atomo;
}

int Atomo::get_numero_atomo(){
    return this->numero_atomo;
}

void Atomo::set_coordenada(Coordenada coordenada){
    this->coordenada = coordenada;
}

Coordenada Atomo::get_coordenada(){
    return this->coordenada;
}
