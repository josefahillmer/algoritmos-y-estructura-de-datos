# Guía 0 - Unidad 1

Dado el diagrama de clases, escriba un programa en C++ que implemente dichas clases. El programa permite el ingreso de una lista de proteı́nas con sus caracteristicas como: Nombre de la proteina, id, cadenas, numero y nombre de aminoacidos, numero y nombre de atomos y coordenadas.

# Empezando

Se debe abrir el archivo por una terminar desde la localizacion de la carpeta correspondiente. En el programa se mostrara un menu con 4 opciones las cuales son: ingresar proteína (donde el usuario podra ingressar una proteína con sus correspondiente datos), después se mostrata la opción ver lista de proteínas, la opción de proteínas en el programa (el cual tiene una proteína ingresada por defecto) y por último la opción salir.


# Prerequisitos
# Sistema operativo linux:
De preferencia Ubuntu o Debian
# Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h
# Editor de texto
# Make
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

# Ejecutando las pruebas
Para ejecutar el programa se debe realizar los siguientes comandos:
`g++ programa.cpp Proteina.cpp Cadena.cpp Aminoacido.cpp Atomo.cpp Coordenada.cpp -o programa`
`make`
`./programa`
El programa mostrara un menu con 4 opciones:
- Ingresar proteina: 
Se pedira al usario ingresar una proteina con sus correspondientes datos
- Ver lista de proteinas:
Se mostrara todas las proteinas que el usuario ha ingresado
- Proteinas en el programa:
Se mostrara las proteinas que se encuentran por defecto y además se le ira agregando las proteínas que el usurio ingrese
- Salir:
Cerrara el programa



# Despliegue 
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- C++ : Es un lenguaje de programación
- Librerias: list, iostream

# Autores
- Josefa Hillmer - jhillmer19@alumnos.utalca.cl




