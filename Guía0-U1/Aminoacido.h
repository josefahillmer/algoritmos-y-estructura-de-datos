#include <iostream>
#include <list>
using namespace std;
#include "Atomo.h"

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
    private:
        string nombre_aa = "\0";
        int numero_aa = 0;
        list <Atomo> atomos;
    public:
        Aminoacido(string nombre, int numero);
        // Metodos get, set y add
        void set_nombre_aminoacido(string nombre);
        void set_numero_aminoacido(int numero);
        string get_nombre_aminoacido();
        int get_numero_aminoacido();
        void add_atomo(Atomo atomo);
        list<Atomo> get_atomos();
};
#endif
