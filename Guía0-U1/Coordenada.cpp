#include <iostream>
using namespace std;
#include "Coordenada.h"

// Constructor
Coordenada::Coordenada(){
    float c_x = 0;
    float c_y = 0;
    float c_z = 0;
}

Coordenada::Coordenada(float x, float y, float z){
    this->c_x = x;
    this->c_y = y;
    this->c_z = z;
}

// Metodos get and set
void Coordenada::set_x(float x){
    this->c_x = x;
}

void Coordenada::set_y(float y){
    this->c_y = y;
}

void Coordenada::set_z(float z){
    this->c_z = z;
}

float Coordenada::get_x(){
  return this->c_x;
}

float Coordenada::get_y(){
    return this->c_y;
}

float Coordenada::get_z(){
    return this->c_z;
}
