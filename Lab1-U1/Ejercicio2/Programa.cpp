/*Escriba un programa que lea N frases en un arreglo de caracteres y determine el número de minúsculas
y mayúsculas que hay en cada una de ellas. Puede utilizar las funciones islower() y isupper() de la
librerı́a cctype.*/

#include <list>
#include <iostream>
#include <string>
#include <cctype>
using namespace std;
#include "Frase.h"


int main(){
    //Variables
    int cantidad;
    string oracion;

    cout << "Ingrese el numero de frases que ingresara: ";
    cin >> cantidad;

    // Arreglo que contendra la cantidad de frases
    Frase frases[cantidad];

    // Ciclo que alamacena las frases
    for(int i=0; i<cantidad; i++){
        cout << "Ingrese frase: ";
        cin >> oracion;
        frases[i].set_frase(oracion);
        // Obtiene la cantidad de mayusculas y minusculas de las frases
        frases[i].set_mayusculas();
        frases[i].set_minusculas();

        cout << "Mayusculas: " << frases[i].get_mayusculas() << endl;
        cout << "Minusculas: " << frases[i].get_minusculas() << endl;

      }
    return 0;
}
