#include <list>
#include <iostream>
using namespace std;
#include "Frase.h"

// Constructor de la clase
Frase::Frase(){
    string frase = "\0";
    int mayusculas = 0;
    int minusculas = 0;
}

// Metodo set frase
void Frase::set_frase(string frase){
    this->frase = frase;
}

// Metodo para set mayusculas
void Frase::set_mayusculas(){
    // Ciclo que contara las mayusculas
    for(int i=0; i<frase.size();i++){
        // Ocupamos la funcion isupper
        if(isupper(frase[i])){
            // almacenamos la suma
            this->mayusculas = this->mayusculas + 1;
        }
    }
}

// Metodo set minusculas
void Frase::set_minusculas(){
    // Ciclo que contara las minusculas
    for(int i=0; i<frase.size();i++){
        // En este caso utilizamos la funcion islower
        if(islower(frase[i])){
            this->minusculas = this->minusculas + 1;
        }
    }
}

// Metodos get de frase
string Frase::get_frase(){
    return this->frase;
}

// Metodo get de mayuscula
int Frase::get_mayusculas(){
    return this->mayusculas;
}

// Metodo get minuscula
int Frase::get_minusculas(){
    return this->minusculas;
}
