#include <list>
#include <iostream>
using namespace std;

#ifndef FRASE_H
#define FRASE_H

// Se define la clase de frase
class Frase{
    private:
        string frase = "\0";
        int mayusculas = 0;
        int minusculas = 0;

    public:
        // Constructor
        Frase();

        // Metodos set and get
        void set_frase(string frase);
        void set_mayusculas();
        void set_minusculas();
        string get_frase();
        int get_mayusculas();
        int get_minusculas();
};
#endif
