# Ejercicio 2 "Numero de mayúsculas y minúsculas de una frase"
El proposito de este ejercicio es realizar un programa que lea N frases de un arreglo de caracteres y determinar el número de minúsculas
y mayúsculas que hay en cada una de ellas. Utilizando las funciones islower() y isupper() de la librerı́a cctype.

# Empezando
Se deben abrir el ejercicios por una terminar desde la localizacion de la carpeta correspondiente. 
En el segundo ejercicio se pedira al usuario ingresar la cantidad de frases y la frase correspondientes para luego imprimir la cantidad de mayúsculas y minúsculas que tiene cada una de las frases ingresadas.

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h

- Editor de texto

- Make
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

# Ejecutando las pruebas
Para ejecutar los programas se debe realizar los siguientes comandos:
- Ejercicio 2:
`g++ programa.cpp Programa.cpp Frase.cpp -o programa`
`make`
`./programa`

# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación

# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl