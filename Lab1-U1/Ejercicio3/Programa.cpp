/*Una empresa registra para cada uno de sus N clientes los siguientes datos:
Nombre (cadena de caracteres)
Teléfono (cadena de caracteres)
Saldo (entero)
Moroso (booleano)
Escriba un programa que permita el ingreso de los datos de los clientes y luego permita imprimir sus
datos indicando su estado (morosos o no). Utilice arreglo de clases en su solución.*/

#include <iostream>
using namespace std;
#include "Persona.h"


// Funcion que ingresa los clientes al arreglo Persona
void ingresar_cliente(Persona *personas, int cantidad){
    // Variables
    string nombre, telefono;
    int saldo, moroso;

    cout << "-----------------------------------------" << endl;
    cout << "INGRESE INFORMACIÓN DEL CLIENTE" << endl;
    // Ciclo para pedir informacion de cada uno de los clientes
    for(int i=0; i<cantidad; i++){
        cout << "Ingrese el nombre del cliente: ";
        cin >> nombre;
        cout << "Ingrese el numero del cliente: ";
        cin >> telefono;
        cout << "Ingrese el saldo del cliente: ";
        cin >> saldo;
        // Se agrega los datos al arreglo
        personas[i].set_nombre(nombre);
        personas[i].set_telefono(telefono);
        personas[i].set_saldo(saldo);

        cout << "¿El clinte es moroso? 1. Si / 2. No: ";
        cin >> moroso;
        // Se agrega moroso al arreglo
        if (moroso == 1){
            personas[i].set_moroso(1);
        }else{
            personas[i].set_moroso(0);
        }

    }

}

// Fucnion que imprime los datos de los clientes
void imprimir_datos(Persona *personas, int cantidad){

    cout << "LISTA DE CLIENTES" << endl;
    // Ciclo que recorre el arreglo y los imprime
    for(int i=0; i<cantidad; i++){
      cout << "-----------------------------------------------" << endl;
      cout << "Nombre: " << personas[i].get_nombre() << endl;
      cout << "Telefono: " << personas[i].get_telefono() << endl;
      cout << "Saldo: " << personas[i].get_saldo() << endl;
      if(personas[i].get_moroso() == 1){
        cout << "Moroso: Si" << endl;
      }else{
          cout << "Moroso: No" << endl;
      }
    }
}

// Funcion principal
int main(){
    //Variable
    int cantidad;

    cout << "BIENVENIDO" << endl;

    cout << "Ingrese la cantidad de clientes: ";
    cin >> cantidad;
    // Se crea un arreglo Persona
    Persona personas[cantidad];
    ingresar_cliente(personas, cantidad);
    imprimir_datos(personas, cantidad);


  return 0;

}
