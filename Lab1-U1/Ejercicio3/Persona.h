using namespace std;

#ifndef PERSONA_H
#define PERSONA_H

// Clase persona
class Persona {
    private:
        string nombre = "\0";
        string telefono = "\0";
        int saldo = 0;
        bool moroso = 0;
    public:
        // constructor
        Persona();

        // Metodos set y get_moroso
        void set_nombre(string nombre);
        void set_telefono(string telefono);
        void set_saldo(int saldo);
        void set_moroso(bool moroso);
        string get_nombre();
        string get_telefono();
        int get_saldo();
        bool get_moroso();

};
#endif
