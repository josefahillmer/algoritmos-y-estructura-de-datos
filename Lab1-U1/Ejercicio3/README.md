# Ejercicio 3 "Empresa registrando datos de clientes"
En el ejercicio se tendra una empresa que registra para cada uno de sus N clientes los siguientes datos: Nombre (cadena de caracteres), Teléfono (cadena de caracteres), Saldo (entero), Moroso (booleano), el proposito de esto es realizar un programa que permita el ingreso de los datos de los clientes y luego permita imprimir sus datos indicando su estado (morosos o no). Utilizando arreglo de clases en su solución.
 que tendra una empresa registra para cada uno de sus N clientes, la cual tendra los: Nombre (cadena de caracteres), teléfono (cadena de caracteres), saldo (entero), moroso (booleano).

# Empezando
Se deben abrir los ejercicios por una terminar desde la localizacion de la carpeta correspondiente. 
En el ejercicio se pedira cuantos clientes decea ingresar al sistema, despúes se le pedire al usuario inscribir cada uno de los datos del cliente: Nombre, telefono, saldo y moroso. Luego se imprimera los datos correspondientes a cada usuario.

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h

- Editor de texto

- Make
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

# Ejecutando las pruebas
Para ejecutar los programas se debe realizar los siguientes comandos:
- Ejercicio 3:
`g++ programa.cpp Programa.cpp Persona.cpp -o programa`
`make`
`./programa`

# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación

# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl