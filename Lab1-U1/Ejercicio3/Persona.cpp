#include <iostream>
using namespace std;
#include "Persona.h"

// constructor
Persona::Persona(){
    string nombre = "\0";
    string telefono = "\0";
    int saldo = 0;
    bool moroso = 0;
}

// Metodo set del nombre
void Persona::set_nombre(string nombre){
    this->nombre = nombre;
}

// Metodo set telefono
void Persona::set_telefono(string telefono){
    this->telefono = telefono;
}

// Metodo set saldo
void Persona::set_saldo(int saldo){
    this->saldo = saldo;
}

// Metodo set moroso
void Persona::set_moroso(bool moroso){
    this->moroso = moroso;
}

// Metodos get del nombre
string Persona::get_nombre(){
    return this->nombre;
}

// Metodo get telefono
string Persona::get_telefono(){
    return this->telefono;
}

// Metodo get saldo
int Persona::get_saldo(){
    return this->saldo;
}

// Metodo get moroso
bool Persona::get_moroso(){
    return this->moroso;
}
