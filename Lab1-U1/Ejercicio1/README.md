# Ejercicio 1 "Suma del cuadrado de un numero"
El proposito de este ejercicio es realizar un codigo en  lenguaje C ++, mediante la implementacion de clases crear un programa en el cual se llenara un areglo de caracter unidimensional con numeros de N enteros para luego ejecutar la accion de calcular la suma del cuadrado de los numeros.

# Empezando
Se deben abrir los ejercicios por una terminar desde la localizacion de la carpeta correspondiente. 
Al abrir el ejercicio se pedira al usuario la cantidad de numeros a ingresar y los numeros correspondientes para luego imprimer la suma de sus cuadrados.

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h

- Editor de texto

- Make
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

# Ejecutando las pruebas
Para ejecutar los programas se debe realizar los siguientes comandos:
- Ejercicio 1:
`g++ programa.cpp Programa.cpp Calculo.cpp -o programa`
`make`
`./programa`

# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación


# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl