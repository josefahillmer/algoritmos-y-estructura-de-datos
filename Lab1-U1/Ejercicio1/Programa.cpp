/*Escriba un programa que llene un arreglo unidimensional de números enteros y luego obtenga como
resultado la suma del cuadrado de los números ingresados. (Considere un arreglo unidimensional de
tipo entero de N elementos).*/

#include <iostream>
using namespace std;
#include "Calculo.h"


// Funcion principal
int main(){
    // Variables
    int cantidad;
    int cifra;

    cout << "Ingrese cantidad de numeros a ingresar: ";
    cin >> cantidad;
    // Arreglo unidimensinal de cantidad de numeros ingresados
    int arreglo[cantidad];

    // Ciclo que agrega los numeros
    for(int i=0; i<cantidad; i++){
        cout << "Ingrese numero: ";
        cin >> cifra;
        // Se agrega el numero a Calculo
        Calculo num = Calculo();
        // Se obtiene el cuadrado
        num.set_numero(cifra);
        num.set_num_cuadrado();
        // Se guarda en el arreglo
        arreglo[i] = num.get_num_cuadrado();
    }

    // Variable
    int suma=0;
    // Ciclo que suma los cuadrados de los numeros
    for(int i=0; i<cantidad; i++){
        suma = arreglo[i] + suma;
    }

    cout << "La suma de los cuadrados es: " << suma << endl;

    return 0;
}
