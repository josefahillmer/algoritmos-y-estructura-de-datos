#include <iostream>
using namespace std;
#include "Calculo.h"

// Constructor
Calculo::Calculo(){
    int numero = 0;
    int num_cuadrado = 0;
}

// Metodo para obtener el numero
void Calculo::set_numero(int numero){
    this->numero = numero;
}

// Metodo para obtener el cuadrado
void Calculo::set_num_cuadrado(){
  // Se genera el cuadrado del numero
    this->num_cuadrado = (this-> numero * this->numero);
}

// Metodo que nos devuelve el numero
int Calculo::get_numero(){
    return this->numero;
}

// Metodo que devuelve el cuadrado
int Calculo::get_num_cuadrado(){
    return this-> num_cuadrado;
}
