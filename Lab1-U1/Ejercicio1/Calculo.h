using namespace std;

#ifndef CALCULO_H
#define CALCULO_H

// Se define la clase elemento
class Calculo {

    private:
        int numero = 0;
        int num_cuadrado = 0;

    public:
        // Constructor
        Calculo();

        // Metodos set and get
        void set_numero(int numero);
        void set_num_cuadrado();
        int get_numero();
        int get_num_cuadrado();

};
#endif
