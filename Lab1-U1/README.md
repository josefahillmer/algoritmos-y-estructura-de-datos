# Laboratiorio 1 - Unidad 1
El laboratorio consta de tres ejercicios los cuales son:
- Ejercicio 1: Un programa que se utilizara un arreglo unidimensional de números enteros y luego se obtendra la suma del cuadrado de los números ingresados.
- Ejercicio 2: Un programa leara N frases en un arreglo de caracteres y se determinara número de minúsculas
y mayúsculas que hay en cada una de ellas. 
- Ejercicio 3: Un programa que tendra una empresa registra para cada uno de sus N clientes, la cual tendra los: Nombre (cadena de caracteres), teléfono (cadena de caracteres), saldo (entero), moroso (booleano).

# Empezando
Se deben abrir los ejercicios por una terminar desde la localizacion de la carpeta correspondiente. 
En el primer ejercicio se pedira al usuario ingresar la cantidad de numeros a ingresar y los numeros correspondientes para luego imprimer la suma de sus cuadrados.

En el segundo ejercicio se pedira la cantidad de frases que ingresara y luego sus frases correpondientes para luego imprimir la cantidad de mayúsculas y minúsculas que tendra cada frase.

En el ejercicio 3 se pedira cuantos clientes decea ingresar al sistema, despúes se le pedire los datos para ingresarlos los cuales son: Nombre, telefono, saldo y moroso. Luego se imprimera los datos correspondientes a cada usuario.

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h

- Editor de texto

- Make
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

# Ejecutando las pruebas
Para ejecutar los programas se debe realizar los siguientes comandos:
- Ejercicio 1:
`g++ programa.cpp Programa.cpp Calculo.cpp -o programa`
`make`
`./programa`

- Ejercicio 2:
`g++ programa.cpp Programa.cpp Frase.cpp -o programa`
`make`
`./programa`

- Ejercicio 3:
`g++ programa.cpp Programa.cpp Persona.cpp -o programa`
`make`
`./programa`

# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación


# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl