#include <iostream>
using namespace std;
#include "Pila.h"
#include "Contenedor.h"

// Constructor
Pila::Pila(int max){
    this-> max = max;
    this-> arreglo_pila = new Contenedor [this->max];

}

// Metodo para observar si la pila se encuntra vacia
void Pila::Pila_vacia(){
    if(this-> tope == 0){
        // Pila vacía
        this-> band = true;
    }else{
      // La pila está llena
      this-> band = false;
    }
}

// Metodo para observar si la pila se encuntra llena
void Pila::Pila_llena(){
    if(this-> tope ==  this-> max){
        // Pila vacía
        this-> band = true;
    }
    else{
        // Aun no esta llena
        this-> band = false;
    }
}

// Metodo para agregar los datos de los contenedores// Metodo para eliminar datos de la pila
void Pila::push(){
    // Ver si la pila no esta llena, con un booleano false
    Pila_llena();
    if(this-> band == false){
        this-> tope = this-> tope + 1;
        //Se agrega los contenedores
        this-> arreglo_pila[this->tope - 1].add_datos();
        cout << "Dato agregado" << endl;
    // Si el booleano es true la pila se encuntra llena
    }else{
        cout << "No se puede agregar el dato, pila llena" << endl;
    }
    cout << endl;
}

// Metodo para eliminar datos de la pila
void Pila::pop(string nombre, string empresa, int n){
    // Ver si la pila se encuntra vacía, con un booleano false
    Pila_vacia();
    if(this-> band == false){
        for(int i=0; i<n;i++){
          for(int j=0; i<this->max;i++){
              if(this->arreglo_pila[this->tope-1].get_nombre() == nombre && this->arreglo_pila[this->tope-1].get_empresa() == empresa){
                  cout << "Se ha eliminado :" << "|" << this-> arreglo_pila[tope-1].get_nombre() << " -- " << arreglo_pila[tope-1].get_empresa()<< "|" << endl;
                  this-> tope = this-> tope -1;
              }
            }
          }
    }else{
        cout << "La pila está vacía, nada para eliminar"<< endl;
    }
    cout << endl;
}

// Metodo que imprime los elementos
void Pila::imprimir_pila(){
  // Imprimimos los datos
  cout << '\t';
  for(int i = this->tope; i >= 1; i--){
      cout << "|" << arreglo_pila[i-1].get_nombre() << "--" << arreglo_pila[i-1].get_empresa() << "|";
    }
  cout << "\n";
}
