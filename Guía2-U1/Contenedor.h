#include <iostream>
using namespace std;

#ifndef CONTENEDOR_H
#define CONTENEDOR_H

class Contenedor{
    // Atributos privados
    private:
        string nombre;
        string empresa;

    // Atributos públicos
    public:
        Contenedor();
        void add_datos();
        void set_nombre(string nombre);
        void set_empresa(string empresa);
        string get_nombre();
        string get_empresa();

};
#endif
