#include <iostream>
#include "Contenedor.h"
using namespace std;

// Constructor
Contenedor::Contenedor(){
}

// Método que pide los datos del contenedor
void Contenedor::add_datos(){
    cout << "Ingrese el nombre del contenedor: ";
    getline(cin, this->nombre);

    cout << "Ingrese la empresa del contenedor: ";
    getline(cin, this->empresa);
}

// Metodos para setear el nombre
void Contenedor::set_nombre(string nombre){
    this->nombre = nombre;
}

// Metodo para setear la empresa
void Contenedor::set_empresa(string empresa){
    this->empresa = empresa;
}

// Metodo para obtner el nombre
string Contenedor::get_nombre(){
    return this->nombre;
}

// Metodo para obtner la empresa
string Contenedor::get_empresa(){
    return this->empresa;
}
