#include <iostream>
#include "Pila.h"
#include "Contenedor.h"

using namespace std;

// Método que imprimira el puerto
void imprimir_puerto(int n, Pila **puerto){
    // Se imprime la pila de forma horizontalmente
    // Recorre los contenedores
    for(int i = 0; i < n; i++){
        puerto[i]-> imprimir_pila();
    }
}

// Método que llenara el puerto
void llenar_puerto(int n, int max, Pila **puerto){
    // Recorre los contenedores
    for(int i = 0; i < n; i++){
        puerto[i] = new Pila(max);
        // Recorre el tamaño de la pila
        for (int j = 0; j < max; j++){
            puerto[i]-> push();
        }
    }
}

void eliminar(Pila **puerto, int max, int n){
    int columna;
    string nombre;
    //string line;
    string empresa;
    Contenedor contenedor = Contenedor();
    // Se imprime el puerto para que el usuario vea que contenedor desea Eliminar
    cout << "PUERTO: " << endl;
    imprimir_puerto(n, puerto);
    cout << endl;
    // Esto sucede debido a que se necesita un espacio en blanco en las pilas para poder eliminar y mover los contenedores
    cout << "Para empezar a eliminar si o si debe ingresar el primer elemento que se encuentra,";
    cout << " el que esta en la esquina derecha de los contenedores\n" << endl;
      // Datos del contenedor que desea eliminar
    cout << "Ingrese el nombre del contenedor: ";
    getline(cin, nombre);
    cout << "Ingrese la empresa del contenedor: ";
    getline(cin, empresa);
    // Ciclo para llamar a la funcion pop
    for(int i = 0; i < n; i++){
        puerto[i] -> pop(nombre, empresa, n);
    }
}


// método menú
void menu(Pila **puerto, int max, int n){

    int opcion;
    // Se imprimen las opciones del usuario
    cout << "\nMENU\n" << endl;
    cout << "|1| Agregar contenedor" << endl;
    cout << "|2| Eliminar contenedor" << endl;
    cout << "|3| Ver contenedores." << endl;
    cout << "|4| Salir." << endl;
    cout << "\nIngrese opción: ";
    cin >> opcion;

    system("clear");

    while(getchar() != '\n');

    switch(opcion){
        case 1:
            // Metodo para llenar el puerto seco
            llenar_puerto(n, max, puerto);
            menu(puerto, max, n);
            break;

        case 2:
            // Metodo que removera datos
            eliminar(puerto, max, n);
            menu(puerto, max, n);
            break;

        case 3:
            // Metodo que imprime los datos
            imprimir_puerto(n, puerto);
            menu(puerto, max, n);
            break;

        case 4:
            cout << "Bye" << endl;
            break;

    }
}

// Funcion principal o main
int main(){
    // Variables
    // El tamaño de la Pila
    int max = 0;
    // La cantidad de los contenedores
    int n;
    Pila *puerto[n];

    cout << "-------------------------------" << endl;
    cout << "\tPUERTO SECO" << endl;
    cout << "-------------------------------" << endl;

    cout << "Ingrese la cantidad de contenedores: ";
    cin >> n;

    cout << "Ingrese el tamaño máximo de la pila: ";
    cin >> max;

    menu(puerto, max, n);

    return 0;
}
