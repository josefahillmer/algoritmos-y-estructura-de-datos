#include <iostream>
#include "Contenedor.h"

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{
    // Atributos privados
    private:
        int tope = 0;
        int max;
        Contenedor *arreglo_pila = NULL;
        bool band;


    // Atributos publicos
    public:
        Pila(int max);
        // Métodos pila vacía, llena, push, pop y imprimir
        void Pila_vacia();
        void Pila_llena();
        void push();
        void pop(string nombre, string empresa, int n);
        void imprimir_pila();
};
#endif
