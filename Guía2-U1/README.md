# Guía 2 - Unidad 1 "Puerto seco donde se guarda mercaderı́a en contenedores"
El programa consiste en la creación de un puerto seco que contendra pilas de contenedores, inicialemnte se pedira la cantidad de contenedores y el máximo de la pila. Donde se permita gestionar el ingreso y salida de contenedores. Note que para retirar un contenedor es necesario retirar los contenedores que están encima de él y colocarlos en otra pila. 

# Empezando
Al comenzar el programa se pedira al usuario ingresar la cantidad de contenedores que tendra el puerto seco y el maáximo de las pilas. Luego se mostrar un ménu con 4 opciones: agregar contenedor, eliminar contenedor, ver contenedores y salir.

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h

- Editor de texto

- Make
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

# Ejecutando las pruebas
Para ejecutar el programa se debe realizar los siguientes comandos en la terminal con la carpeta correspondiente:
`g++ programa.cpp Programa.cpp Pila.cpp -o programa`

`make`

`./programa`

El programa solicitará el ingreso de la capacidad de contenedores que tendra el puerto y la capacidad máxima que tendrá la pila. (Si ingresa una letra o un numero negativa el programa se terminara). Luego se mostrara un menú con las siguientes opciones:
1. Agregar contenedor: Se pedira los datos para rellenar el puerto según la cantidad de contenedores y el máximo de este (por ejemplo si ingresa que la cantidad de contenedores son n y el máximo de este son m entonces le pedira nxm datos), debera ingresar el nombre y la empresa de cada contenedor. Cada vez que ingrese un dato se imprimira que el dato fue agregado. Si ingresa un numero el programa se terminara.
2. Eliminar contenedor: Al precionar está opción se mostrara los datos que contendra el puerto (que en la parte de imprimir contenedores se explicara como se mostraran), para eliminar se tendra una primera condición el cual es que si o si debe eliminar el primer elemento que se encuentra, el que está en la esquina derecha de los contenedores, esto es debido a como está el puerto lleno se tendrá que eliminar uno para mover los otros. Luego para poder eliminar se pedira el nombre del contenedor y la empresa de este. Cuando es eliminado con éxito se mostrara un mensaje de elemento eliminado. Al momento de eliminar los datos se moveran hacia la derecha.
3. Ver contenedores: Se mostraran los contenedores de forma horizontal es decir si n es 2 y m es 3 entonces se mostrara de la siguiente manera:    

|-C3-C2-C1-|

|-C6-C5-C4-|

4. Salir: Se cerrara el programa
Todas las opciones anteriores, excepto salir, retornaran al menú.

# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación

# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl