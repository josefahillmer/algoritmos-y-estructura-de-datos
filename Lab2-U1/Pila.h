using namespace std;

#ifndef PILA_H
#define PILA_H

// Se define la clase pila
class Pila{
    private:

        int max = 0;
        int *arreglo = NULL;
        int tope = 0;
        bool band;

    public:
        // Constructor
        Pila(int max, int tope);
        // Metodo de pila vacia y llena y para agregar, remover e imprimir la pila
        void Pila_vacia();
        void Pila_llena();
        void push(int dato);
        void pop();
        void imprimir();
};
#endif
