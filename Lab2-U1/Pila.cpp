#include <iostream>
//#include <stdlib.h>
using namespace std;
#include "Pila.h"

// Constructor
Pila::Pila(int max, int tope){
    this-> max = max;
    this-> arreglo = new int[max];
    this-> tope = tope;
    bool band;
    this-> band = band;

}

void Pila::Pila_llena(){
    /* Para que la pila este llena debe ser igual a tope +1
    de esta forma los primeros indices seran 1 y 2 */
    if(this-> tope + 1 ==  this-> max){
        this-> band = true;
    }
    else{
        // Aun no esta llena
        this-> band = false;
    }
}

void Pila::Pila_vacia(){
    // Si el tope es igual a -1 es porque está vacía
    if(this-> tope == -1){
        this-> band = true;
    }else{
      this-> band = false;
    }
}

void Pila::push(int dato){
    // Ver si la pila no esta llena, con un booleano false
    Pila_llena();
    if(this-> band == false){
        this-> tope = this-> tope + 1;
        //Se agrega al Arreglo
        this-> arreglo[this->tope] = dato;
        cout << "Dato agregado" << endl;
    }else{
        cout << "No se puede agregar el dato, pila llena" << endl;
    }
    cout << endl;
}

void Pila::pop(){
    // Ver si la pila se encuntra vacía, con un booleano false
    Pila_vacia();
    if(this-> band == false){
        cout << "Se ha eliminado [" << this-> arreglo[tope] << "]" << endl;
        // Se cambia el tope
        this-> tope = this-> tope -1;
    }else{
        cout << "La pila está vacía, nada para eliminar"<< endl;
    }
    cout << endl;
  }

void Pila::imprimir(){
  // Imprimimos los datos
  cout << "-----------------------------------" << endl;
  for(int i=this->tope; i>-1 ;i--){
      cout << "|" << this-> arreglo[i] << "|" << endl;
  }
  cout << "-----------------------------------" << endl;
}
