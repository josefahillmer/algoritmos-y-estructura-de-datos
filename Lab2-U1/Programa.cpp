#include <iostream>
using namespace std;
#include "Pila.h"


void menu(Pila pila){

    int opcion;
    // Se imprimen las opciones del usuario
    cout << "Ingrese una de las siguientes opciones: " << endl;
    cout << "|1| Agregar elemento a pila." << endl;
    cout << "|2| Eliminar elemento de la pila." << endl;
    cout << "|3| Ver pila." << endl;
    cout << "|4| Salir." << endl;
    cin >> opcion;

    if(opcion == 1){
        int dato;
        cout << "Ingrese dato a agregar: ";
        cin >> dato;
        // Se agrega a la pila el elemento
        pila.push(dato);
        menu(pila);
    }
    else if(opcion == 2){
        // Metodo que removera datos
        pila.pop();
        menu(pila);
    }
    else if(opcion == 3){
        // Metodo que imprime los datos
        pila.imprimir();
        menu(pila);
    }
    else if(opcion == 4){
        cout << "Bye" << endl;
    }
    else{
        cout << "Opción invalida" << endl;
    }

}

int main(){
    // Variable
    int max;
    cout << "Capacidad máxima de la pila: ";
    cin >> max;

    if(max > 0){
      // Se debe comenzar con -1
      int tope = -1;
      Pila pila = Pila(max, tope);
      menu(pila);

    }else{
        cout << "Capacidad invalida" << endl;
    }

  return 0;
}
