# Laboratiorio 2 - Unidad 1 "Pilas y Colas Circulares con Arreglos"
Algoritmo para manipular y mostrar una pila. Se debe definir la capacidad máxima de la pila, donde se va a poder agregar o eliminar elementos. Al momento de eliminar datos, el primero que sale es el último que entra a la pila, por esta razón son cierculares.

# Empezando
Se deben abrir los ejercicios por una terminar desde la localizacion de la carpeta correspondiente. 
Se pedira la capacidad máxima de la pila. Despúes se mostrara un menu con 4 opciones las cuales son: agregar elemento a pila, eliminar elemento de la pila, ver pila y salir del programa.

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h

- Editor de texto

- Make
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

# Ejecutando las pruebas
Para ejecutar el programa se debe realizar los siguientes comandos en la terminal con la carpeta correspondiente:
`g++ programa.cpp Programa.cpp Pila.cpp -o programa`

`make`

`./programa`

El programa solicitará el ingreso de la capacidad máxima que tendrá la pila. (Si ingresa una letra o un numero negativa el programa se terminara). Luego se mostrara un menú con las siguientes opciones:
Posterior al ingreso de la capacidad de la pila se nos abrirá un menú con las siguientes opciones:
1. Agregar elemento a la pila: Se solicitara el elemento que quiere ingresar a la pila. Pero solo se agregará cuando la pila tenga espacio para agregar más elementos, es decir cuando la pila no esté llena. Si la pila se encuentra llena se nos avisara y se retornora al menú.
2. Eliminar elemento de la pila: Esta opción eliminará automáticamente el último elemento de la pila. Si la pila está vacía se nos avisara y retornara al menú.
3. Ver pila: Esta opción mostrará la pila con sus datos. El primero elemento ingresado estara de los últimos y el elemento que fue ingresado recientemente de los primeros.
4. Salir: Salir del programa.
Si ingresa cualquier otra opción el programa se detendra.

# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación

# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl