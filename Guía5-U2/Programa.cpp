#include <iostream>
#include <unistd.h>
#include <fstream>
#include "Arbol.h"

#define TRUE 1
#define FALSE 0

using namespace std;

// Funcion pricipal y menú
int main(){

		string elemento;
		string elemento2;
		int opcion;
		// Se abrira el archivo de pdbs
		ifstream archivo("pdbs.txt");
		string linea;
		// Se crea arbol vacio
		Nodo *raiz = NULL;
		// Y creamos nuestro arbol binario
	 	Arbol avl = Arbol();

		// Indica si la altura del arbol ha crecido
		int inicio = FALSE;

    system("clear");

	   do{
		     cout << "\t MENÚ" << endl;
				 cout << " [1] Insertar pdbs" << endl;
		     cout << " [2] Insertar un nuevo Nodo" << endl;
         cout << " [3] Buscar un elemento" << endl;
         cout << " [4] Eliminar un Nodo" << endl;
         cout << " [5] Modificar un Nodo" << endl;
         cout << " [6] Generar imagen" << endl;
         cout << " [7] Salir" << endl;
         cout << "\nIngrese su opción: ";
         cin >> opcion;

		switch(opcion){

			case 1:
					system("clear");
					// Se abre el archivo
					while(getline(archivo, linea)){
							// Se insertaran los elementos al grafo
							/* Solo se insertaran 150 elementos*/
							avl.InsercionBalanceado(raiz, &inicio, linea);
					}
					avl.crear_grafico(raiz);
					break;

			case 2:
					system("clear");
          cout << "Ingrese elemento: ";
					cin >> elemento;
					// Se insertara elementos al árbol
					avl.InsercionBalanceado(raiz, &inicio, elemento);

				  break;

			case 3:
					system("clear");
					cout << "Ingrese elemento a buscar: ";
					cin >> elemento;
					// Se buscara elementos en el árbol
					avl.Busqueda(raiz, elemento);
          break;

			case 4:
					system("clear");
          cout << "Ingrese el elemento que desea eliminar: ";
          cin >> elemento;
					// Se eliminara un elemento
					avl.EliminacionBalanceado(raiz, &inicio, elemento);
					break;

			case 5:
					// Se modificara un elemento del árbol
					/* Primero se ve si se encuentra el elemento, luego se elimina
					y finalmente se modifica */
					system("clear");
          cout << "Ingrese el elemento a modificar: ";
          cin >> elemento;

					cout << "Ingrese el nuevo elemento: ";
					cin >> elemento2;

					// Se llama la funcion eliminar
					avl.EliminacionBalanceado(raiz, &inicio, elemento);
					// Se llama la función insertar
					avl.InsercionBalanceado(raiz, &inicio, elemento2);
					cout << "Se ha generado el cambio" << endl;
					break;

			case 6:
					system("clear");
					// Se genera el grafo
					avl.crear_grafico(raiz);
				  break;

			case 7:
				  exit(1);
				  break;
		}
	}
	while(opcion != 8);
	return 0;
}
