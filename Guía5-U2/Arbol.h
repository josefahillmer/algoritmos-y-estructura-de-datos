#include <iostream>
#include <unistd.h>
#include <fstream>

using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

//Estructura para Nodo
typedef struct _Nodo{
	string info;
  int FE;
	struct _Nodo *izq;
	struct _Nodo *der;
} Nodo;

// Se define la clase arbol
class Arbol{

    // Atributos privados de la clase
    private:
				// Nodos utilizados en las clases
				// Utilizados en la reestructuracion
        Nodo *nodo1 = new Nodo;
        Nodo *nodo2 = new Nodo;
				// Utilizados en la eliminación
        Nodo *aux = NULL;
        Nodo *aux1 = NULL;
	      Nodo *aux2 = NULL;

    // Atributos publicos de la clase
    public:
        // Se define el constructor de la clase
        Arbol();

        // Metodo para crear el Nodo
				// Inserción de nodos
        void InsercionBalanceado(Nodo *&nodocabeza, int *BO, string infor);
				// Busqueda de nodos
        void Busqueda(Nodo *nodo, string infor);
				// Reestructuracion izquierda y derecha
        void Restructura1(Nodo *&nodo, int *BO);
        void Restructura2(Nodo *&nodo, int *BO);
				// Elimina nodos
        void EliminacionBalanceado(Nodo *&nodo, int *BO, string infor);

        // Metodos para crear el grafico
        void crear_grafico(Nodo *p);
        void recorrer_arbol(Nodo *p, ofstream &fp);

};
#endif
