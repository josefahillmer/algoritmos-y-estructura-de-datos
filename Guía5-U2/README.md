# Guía 5 - Unidad 2 "Árboles Balanceados AVL"
El programa cuenta con la creación de un arbol binario balanceado, en el cual el usuario puede insertar id's de proteinas, insertar un nuevo nodo; en esta opción solo se agregaran 150 id's, buscar un elemento, eliminar id's de proteinas, modificar un elemento generando una eliminación y luego la inserción, y además cuenta con la opción de generar un grafo con su árbol en formato png.

# Empezando
Se deben abrir los ejercicios por una terminar desde la localizacion de la carpeta correspondiente. 
Se mostrara un menú con 7 opciones: insertar pdbs, insertar un nuevo nodo, buscar un elemento, modificar un nodo, generar imagen y salir. 

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h

- Editor de texto

- Make:
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

- Se debe tener instalado el paquete:  graphviz

Para instalar en Debian, como usuario root ejecutar:
`sudo apt-get install graphviz`

- Para generar la imagen a partir del archivo fuente datos.txt. 
En la línea de comandos ejecutar:
`$ dot -Tpng -ografo.png datos.txt`
Generará el archivo con la imagem grafo.png

Para visualizar la imagen, se puede ejecutar lo siguiente:
`$ eog grafo.png`

# Ejecutando las pruebas
Para ejecutar el programa se debe realizar los siguientes comandos en la terminal con la carpeta correspondiente:
`g++ Programa.cpp Arbol.cpp -o Programa`

`make`

`./Programa`

El programa mostrara un menú con las siguientes opciones:

**1. Insertar pdbs:** Se insertaran 150 id's de proteínas, que vienen de un archivo llamado pdbs.txt

**2. Insertar un nuevo nodo:** Se pedira al usuario ingresar un elemento, al momento de insertarlo volvera al menú principal.

**3. Buscar un elemento:** Se pedira al usuario ingresar el elemento que desea buscar, si el programa encuentra o no se lo informará al usuario.

**4. Eliminar nodo:** Se pedira al usuario el nodo que desea eliminar. Lo eliminara dependiendo si es un nodo sin hijos, un nodo con un hijo o un nodo con dos hijos. 

**5. Modificar nodo:** Se pedira al usuario eliminar el elemento que quiere modificar y luego insertar un nuevo elemento. 

**6. Monstrar grafo:** Se abrira un archivo llamado grafo.png donde se monstrara el árbol balanceado correspondiente con los id's de las proteínas. 

**7. Salir:** El programa se cerrara, se recomienda cerrar la imagen generada si volvera a probar el programa. 

# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación

# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl