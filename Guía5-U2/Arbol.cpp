#include <iostream>
#include <fstream>
#include <unistd.h>
#include "Arbol.h"

#define TRUE 1
#define FALSE 0

ofstream fp;

// Constructor por defecto de la clase árbol
Arbol::Arbol(){

}

// Procedimiento para insertar un elemento al árbol
void Arbol::InsercionBalanceado(Nodo *&nodocabeza, int *BO, string infor){
	Nodo *nodo = NULL;
  Nodo *nodo1 = NULL;
  Nodo *nodo2 = NULL;
	//temporal
	nodo = nodocabeza;

	// Si el Nodo es distinto a vacío se puede insertar
  if (nodo != NULL) {
		// Si el elemento es menor a la raiz, insertar en rama izquierda
    if (infor.compare(nodo->info) == -1) {
			// Se llama la función insertar nodo
      InsercionBalanceado((nodo->izq), BO, infor);
      // si el bo es verdadero
      if(*BO == TRUE) {
				/* Se analiza el factor de equilibrio para ver si esta balanceado
				el árbol */
        switch (nodo->FE) {
					// FE == 1
          case 1:
						// La derecha y la izquiera deberian tener el mismo nivel
            nodo->FE = 0;
						// boleano es falso
            *BO = FALSE;
            break;

					// FE == 0
          case 0:
						// El puntero apuntara a FE == -1, a la izquierda se añade uno
            nodo->FE = -1;
            break;

					// FE == -1
          case -1:
            /* reestructuración del árbol */
						// El nodo tomara el nodo de la izquierda y se hara una reestructuración
            nodo1 = nodo->izq;

            /* Rotacion II */
						// Si el FE es menor o igual a 0
            if (nodo1->FE <= 0) {
							// EL nodo izquierdo recibe al hijo y el puntero estara en el nuevo y en el antecesor
              nodo->izq = nodo1->der;
              nodo1->der = nodo;
							// Se cambia FE  a 0
              nodo->FE = 0;
              nodo = nodo1;

            } else {
              /* Rotacion ID */
              nodo2 = nodo1->der;
              nodo->izq = nodo2->der;
              nodo2->der = nodo;
              nodo1->der = nodo2->izq;
              nodo2->izq = nodo1;

							// Si el nodo2 es -1 se cambiara a 1
              if(nodo2->FE == -1)
                	nodo->FE = 1;
							// Sino, se cambiara a 0
              else
                nodo->FE = 0;

							// Si el nodo2 es 1 se cambiara a -1
              if (nodo2->FE == 1)
                nodo1->FE = -1;
							//Sino se cambia a 0
              else
                nodo1->FE = 0;

              nodo = nodo2;
            }
            nodo->FE = 0;
            *BO = FALSE;
            break;
        }
      }
		/* Otros casos pueden ser; que se tenga que añadir al árbol en rama
		derecha o no añadir (elemento repetido) */
    }else {
			/* Si el elemento es mayor a la raiz, insertar en rama derecha
			 e indicar su altura */
      if (infor.compare(nodo->info) == 1){
        InsercionBalanceado((nodo->der), BO, infor);

        if (*BO == TRUE) {
					/* Analisis al factor de equilibrio (balance) para determinar que
					 metodo de rotacion utilizar */
          switch (nodo->FE) {
						// FE == -1
            case -1:
              nodo->FE = 0;
              *BO = FALSE;
              break;

						// FE == 0
            case 0:
              nodo->FE = 1;
              break;

						// FE == 1
            case 1:
              /* reestructuración del árbol */
              nodo1 = nodo->der;

              if (nodo1->FE >= 0) {
                /* Rotacion DD */
                nodo->der = nodo1->izq;
                nodo1->izq = nodo;
                nodo->FE = 0;
                nodo = nodo1;

              } else {
                /* Rotacion DI */
                nodo2 = nodo1->izq;
                nodo->der = nodo2->izq;
                nodo2->izq = nodo;
                nodo1->izq = nodo2->der;
                nodo2->der = nodo1;

								// Si nodo2 es igual 1 se cambiara a -1
                if (nodo2->FE == 1)
                  nodo->FE = -1;
								// Sino se cambia a 0
                else
                  nodo->FE = 0;

								// Si nodo2 es igual a -1 se cambia a 1
                if (nodo2->FE == -1)
                  nodo1->FE = 1;
								//Sino se cambia a 0
                else
                  nodo1->FE = 0;

								nodo = nodo2;
              }
              nodo->FE = 0;
              BO = FALSE;
              break;
          }
        }
			/* De lo contrario signifca que el valor que se quiere insertar ya
			existe. Como no se permite la duplicidad de este dato no se
			realiza ningun cambio */
      }else {
        cout << "El Nodo ya se encuentra en el árbol\n" << endl;
      }
    }
	/* Si en Nodo recibido fuera nulo entonces el nuevo Nodo se puede insertar
	en esa posición y se terminan las llamadas recursivas a este método */
  }else{
		// Se crea Nodo
		nodo = new Nodo;
		// Se asigna la infor al Nodo que se desea guardar
		nodo->info = infor;
		// Por defecto, la izquierda y la derecha apuntarán a nulo
		nodo->izq = NULL;
		nodo->der = NULL;
		// Y el factor en 0
		nodo->FE = 0;
    *BO = TRUE;
  }
	nodocabeza = nodo;
}

// Procedimiento para buscar un elemento en el arbol
void Arbol::Busqueda(Nodo *nodo, string infor) {
	// Si el Nodo es distinto a vacío se puede buscar
  if(nodo != NULL){
			// Si la busqueda es menor al Nodo, analiza rama izq
    	if(infor < nodo->info){
      		Busqueda(nodo->izq, infor);
    	}else{
					// Si la busqueda es mayor al Nodo, analiza rama der
      		if(infor > nodo->info) {
        			Busqueda(nodo->der, infor);
					// Se encontro
      	}else{
        		cout << "El Nodo SI se encuentra en el árbol\n" << endl;
      	}
    	}
			// No se encuntra
  }else{
    	cout << "El Nodo NO se encuentra en el árbol\n" << endl;
  }
}

// Procedimineto de reestructuracion izquierda
void Arbol::Restructura1(Nodo *&nodo, int *BO) {
	//bool pasado por ref, indica q la altura de la rama izq ha disminuido
	/* Se usa cuando la altura de la rama izquierda ha disminuido y el factor
	 de equilibrio es 1 */
	Nodo *nodo1, *nodo2;

  if (*BO == TRUE) {
		/* Analisis al factor de equilibrio para ver si esta balanceado
		 el árbol */
    switch(nodo->FE){
      case -1:
        nodo->FE = 0;
        break;

      case 0:
        nodo->FE = 1;
        *BO = FALSE;
        break;

    case 1:
      /* reestructuracion del árbol */
			nodo1 = nodo->der;
			//factor mayor o igual a 0
      if (nodo1->FE >= 0) {
        /* Rotación DD para el proceso de balanceo */
        nodo->der = nodo1->izq;
        nodo1->izq = nodo;

        switch (nodo1->FE) {
          case 0:
            nodo->FE = 1;
            nodo1->FE = -1;
            *BO = FALSE;
            break;
          case 1:
            nodo->FE = 0;
            nodo1->FE = 0;
            *BO = FALSE;
            break;
        }
        nodo = nodo1;
			// Rotación DI  para el proceso de balanceo
      }else{
        nodo2 = nodo1->izq;
        nodo->der = nodo2->izq;
        nodo2->izq = nodo;
        nodo1->izq = nodo2->der;
        nodo2->der = nodo1;

        if (nodo2->FE == 1){
          nodo->FE = -1;
        }else{
          nodo->FE = 0;
				}
        if (nodo2->FE == -1){
          nodo1->FE = 1;
        }else{
          nodo1->FE = 0;
				}
        nodo = nodo2;
        nodo2->FE = 0;
      }
			nodo->FE = 0;
			*BO = FALSE;
      break;
    }
  }
}

// Procedimineto de reestructuración derecha
void Arbol::Restructura2(Nodo *&nodo, int *BO){
	//bool indica q la altura de la rama der ha disminuido
	/* Se usa cuando la altura de la rama derecha ha disminuido y el factor de
	 * equilibrio es -1 */

	Nodo *nodo1, *nodo2;


  if (*BO == TRUE) {
		// Analisis al factor de equilibrio para ver si esta balanceado el árbol
    switch (nodo->FE) {
      case 1:
        nodo->FE = 0;
        break;
      case 0:
        nodo->FE = -1;
        *BO = FALSE;
        break;
      case -1:
        /* reestructuracion del árbol */
        nodo1 = nodo->izq;
				//factor menor o igual a 0
        if (nodo1->FE<=0) {
          /* rotacion II */
          nodo->izq = nodo1->der;
          nodo1->der = nodo;
          switch (nodo1->FE) {
            case 0:
              nodo->FE = -1;
              nodo1->FE = 1;
              *BO = FALSE;
              break;
            case -1:
              nodo->FE = 0;
              nodo1->FE = 0;
              *BO = FALSE;
              break;
          }
          nodo = nodo1;
        } else {
          //Rotación ID para el proceso de balanceo
          nodo2 = nodo1->der;
          nodo->izq = nodo2->der;
          nodo2->der = nodo;
          nodo1->der = nodo2->izq;
          nodo2->izq = nodo1;

          if (nodo2->FE == -1){
            nodo->FE = 1;
          }else{
            nodo->FE = 0;
					}
          if (nodo2->FE == 1){
            nodo1->FE = -1;
          }else{
            nodo1->FE = 0;
        	}
					nodo = nodo2;
					nodo2->FE = 0;
    }
  }
}
}

// Procedimiento que elimina
void Arbol::EliminacionBalanceado(Nodo *&nodo, int *BO, string infor) {
	Nodo *aux, *aux1, *aux2;
	// Si el árbol no está vacío se obtiene el valor de la raiz
  if (nodo != NULL) {
			// Si el elemento es menor o igual a -1, insertar en izq
    	if (infor.compare(nodo->info) <= -1) {
					// Se llama a EliminacionBalanceado pero con el nodo a la izquierda
      		EliminacionBalanceado(nodo->izq, BO, infor);
					// Reestructuracion
      		Restructura1(nodo, BO);
			// Si el elemento es mayoro igual a 1, insertar en derecha
    	}else {
      		if (infor.compare(nodo->info) >= 1) {
					// se llama a EliminacionBalanceado pero con el nodo a la derecha
        	EliminacionBalanceado(nodo->der, BO, infor);
					// reestructuracion
        	Restructura2(nodo, BO);
      		}else {
							// Se verifica antes de eliminar
        			aux = nodo;
							// Nodo se iguala a aux apuntando a izquierda.
        			if (aux->der == NULL) {
          				nodo = aux->izq;
          				*BO = TRUE;
        			}else {
									// Nodo se iguala a aux apuntando a derecha.
          				if (aux->izq==NULL) {
            			nodo = aux->der;
            			*BO=TRUE;
          				}else{
											aux1 = nodo->izq;
											*BO = FALSE;
											// Si es distinto de null, se eliminara
											while(aux1->der != NULL){
													aux2 = aux1;
													aux1 = aux1->der;
													*BO = TRUE;
											}
											nodo->info = aux1->info;
											aux = aux1;
											if(*BO == TRUE){
													aux2->der = aux1->izq;
											}
											else{
													nodo->izq = aux1->izq;
											}
											// Restructuracion con el nodo apuntando a izquierda
            					Restructura2(nodo->izq, BO);
          				}
        			}
							// Se elimina
							delete aux;
							cout << "Nodo eliminado" << endl;
      		}
    	}
  }else{
    cout << "El Nodo NO se encuentra en el árbol\n" << endl;
  }
}

// Procedimineto para generar imagen del arbol
void Arbol::crear_grafico(Nodo *p){
		// Se abre el archivo.
		fp.open("grafo.txt");
		// Escritura en el archivo del arbol
		fp << "digraph G {" << endl;
		fp << "node [style=filled fillcolor=purple];" << endl;

		fp << "nullraiz [shape=point];" << endl;
 		fp << "nullraiz->" << "\"" << p->info << "\"" << " ";
 		fp << "[label=" << p->FE << "];" << endl;

		// llamada a recorrer_arbol para ingresar informacion del arbol
		recorrer_arbol(p, fp);
		fp << "}";
		fp.close();

		// Generacion de la imagen a traves del archivo de texto
		system("dot -Tpng -ografo.png grafo.txt &");
		// Apertura de la imagen
		system("eog grafo.png &");
}

// Funcion que recorre el arbol y crea el archivo.txt
void Arbol::recorrer_arbol(Nodo *p, ofstream &fp){
	// Recorrido del arbol en preorden y se agregan datos al archivo.txt
	if(p != NULL){
		if (p->izq != NULL) {
				fp << "\"" <<  p->info << "\"" << "->" << "\"" << p->izq->info << "\"" << "[label=" << p->izq->FE << "];" << endl;
		} else{
				fp << "\"" << p->info << "i\"" << " [shape=point];" << endl;
				fp << "\"" << p->info << "\"" << "->" << "\"" << p->info << "i\"" << ";" << endl;
		}
		if (p->der != NULL) {
			fp << "\"" << p->info << "\"" << "->" << "\"" << p->der->info << "\"" << "[label=" << p->der->FE << "];" << endl;
		} else{
				fp << "\"" << p->info << "d\"" << " [shape=point];" << endl;
				fp << "\"" << p->info << "\"" << "->" << "\"" << p->info << "d\"" << ";" << endl;

	}
	// Se recorre en izquierda y derecha
	recorrer_arbol(p->izq, fp);
	recorrer_arbol(p->der, fp);
}
}
