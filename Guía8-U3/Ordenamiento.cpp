#include <iostream>
#include <fstream>
#include <string>
#include "Ordenamiento.h"

// se define el TRUE y FALSE
#define TRUE 0
#define FALSE 1

// Constructor
Ordenamiento::Ordenamiento(){

}
// Se imprime el arreglo ordenado
void Ordenamiento::imprimir(int *arreglo, int n){
    for(int i=0; i<n; i++){
      cout << "array["<< i << "]=" << arreglo[i] << " ";
  	}
  	cout << endl;
}

/* Método de selección */
void Ordenamiento::seleccion(int* arreglo, int n){
  // Variable
	int menor;

  // Se busca el elemento más pequeño de la lista.
	for(int i = 0; i < n-1 ; i++) {
		//la info contenida en la posicion i se guarda en un int "menor"
    // Se intercambia con el elemento ubicado en la primera posición de la lista.
		menor = arreglo[i];
		int k = i;

    // Se busca el segundo elemento más pequeño de la lista.
		for(int j = i + 1; j < n; j++){
			if(arreglo[j] < menor){
				//reemplaza la info de menor, con la contenida en la posicion j
        //Se intercambia con el elemento que ocupa la segunda posición en la lista.
				menor = arreglo[j];
				k = j; //igualar variables
			}
		}
		//se reemplaza la info en la posicion k por la que esta en la posicion i
		arreglo[k] = arreglo[i];
		//posicion i se llena con la info contenida en menor
		arreglo[i] = menor;
	}
}

// Método Quicksort
void Ordenamiento::qckSort(int *arreglo, int n) {

  // Variables
	int pilaMenor[n];
	int pilaMayor[n];
	int inicio, fin, pos, tope;
  int izq, der, aux, band;

  tope = 0;
  // Se indica el arreglo desde 0 hasta el indice final
	pilaMenor[tope] = 0;
	pilaMayor[tope] = n - 1;

  /* Mientras qie el elemento se encuntra en orden (Sea mayor que la pila)
  continua disminuyendo el indice */
	while(tope >=0 ){
		inicio = pilaMenor[tope];
		fin = pilaMayor[tope];
		tope =  tope -1;
    // reduce
    izq = inicio;
    der = fin;
    pos = inicio;
    band = TRUE;

    // Mientras no se cruzen los índices
    while (band == TRUE) {
      while ((arreglo[pos] <= arreglo[der]) && (pos != der))
        der = der - 1;

      // Si es igual, no se intercambia
      if (pos == der) {
        band = FALSE;
      // se intecambia
      }else {
        aux = arreglo[pos];
        arreglo[pos] = arreglo[der];
        arreglo[der] = aux;
        pos = der;
        // Si todavía no se cruzan los indices seguimos intercambiando
        while ((arreglo[pos] >= arreglo[izq]) && (pos != izq)){
          izq = izq + 1;
        }
        // Si es igual, no se intercambia
        if (pos == izq) {
          band = FALSE;
        // Se intercambia
        }else{
          aux = arreglo[pos];
          arreglo[pos] = arreglo[izq];
          arreglo[izq] = aux;
          pos = izq;
        }
      }
    }
    // Ordeno la lista de los menores
		if (inicio < (pos - 1)) {
			tope = tope + 1;
			pilaMenor[tope] = inicio;
			pilaMayor[tope] = pos - 1;
		}
    // Ordeno la lista de los mayores
		if(fin > (pos + 1)) {
			tope = tope + 1;
			pilaMenor[tope] = pos + 1;
			pilaMayor[tope] = fin;
		}
	}
}
