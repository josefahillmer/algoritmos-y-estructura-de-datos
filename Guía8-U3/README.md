# Guı́a 8 - Unidad III "Métodos de Ordenamiento Interno"
El programa ordena un mismo conjunto de elementos utilizando los siguientes algortimos de ordenamiento interno: 
- Selección (Método directo O(n²)) 
- Quicksort (Método logarı́tmico O(n x logn))

El programa entrega además los timpos en milisegundos que llevó cada algortimo en ordenar el mismo conjunto de datos iniciales (estos son generados en forma aleatoria, con ayuda de las funciones srand() y rand() de la libreria cstdlib ). Para calcular el tiempo se utiliza la función chrono::high resolution clock::now() de la librerı́a chrono.
Además se pedira ingresar un número de valor n que debe ser ppositibo y menor o igual que 1.000.000 y se pedira un parámetro VER que indicará si se muestra el contenido de los vectores, si quiere que se muestre coloque S y si no N.

# Empezando
Se deben abrir los ejercicios por una terminar desde la localizacion de la carpeta correspondiente. 
Se pidira al usuario ingresar un número y ingresar si quiere mostrar los contenidos de los vectores.

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h

- Editor de texto

- Make:
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.


# Ejecutando las pruebas
Para ejecutar el programa se debe realizar los siguientes comandos en la terminal con la carpeta correspondiente:

`g++ Programa.cpp Ordenamiento.cpp -o Programa`

`make`

`./Programa`

El programa pidira lo siguiente:
- **Ingresar un número:** Este debe ser entre 1 y 1.000.000, si se ingresa un numero menor o mayor a estos se pedira al usuario que reinicie el programa.
- **¿Quiere ver el contenido de los vectores? s/n:** 

S: Está opción mostrara el contenido de los vectores, es decir, los números ordenados de menor a mayor correspondientes a su método.

N: No se mostrará el contenido de los vectores

Una vez ingresado los párametros se mostrara primero el arreglo original y luego una tabla con el método y el tiempo correpondiente a cada uno. Y si quiere mostrar el contenido de los vectores se vera en la parte inferior los elementos ordenados según cada método.
Ejemplo: 

Ingrese un número: 7

¿Quiere ver el contenido de los vectores? s/n: s

    Arreglo original: a[0]=4218 a[1]=2020 a[2]=3548 a[3]=3453 a[4]=4059 a[5]=2457 a[6]=685 a[7]=1521 
    -----------------------------------------
    Metodo       |Tiempo
    -----------------------------------------
    Seleccion    |0 milisegundos
    Quicksort    |0 milisegundos
    -----------------------------------------
    Seleccion    a[0]=685 a[1]=1521 a[2]=1981 a[3]=2020 a[4]=2246 a[5]=2457 a[6]=3453 a[7]=3548

    Quicksort    a[0]=685 a[1]=1521 a[2]=1981 a[3]=2020 a[4]=2246 a[5]=2457 a[6]=3453 a[7]=3548


# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación
- Librerias
- Compilador GNU C++ (g++)

# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl