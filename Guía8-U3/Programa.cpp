#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <chrono>
#include "Ordenamiento.h"

using namespace std;

// Procedimineto que imprime los vectores
void imprimirA(int* arreglo, int* arreglo1, int* arreglo2, Ordenamiento orden, int n){

    // Se imprimen los vectores correspondientes a cada método
    cout << " Seleccion:        ";
    orden.imprimir(arreglo1, n);
    cout << " Quicksort:         ";
    orden.imprimir(arreglo2, n);
    cout << " -----------------------------------------" << endl;
}

// Procedimiento que llena los arreglos y imprime el tiempo de cada método
void llenar(int* arreglo, int n, string ver){
    // se generan las variables de tiempo
    // Ocupamos la libreria de chrono para generar los 2 tiempos para el ordenamiento
    chrono::duration<double> tiempo1, tiempo2;

    // Se inicializa el objeto que contiene los métoso de ordenamiento
    Ordenamiento orden;

    cout << "Arreglo original: ";

    // Se llena el arreglo
    for(int i=0; i < n; i++){
        //Se genera numero aleatorios
        int aleatorio = (rand() % 100000) + 1;
        arreglo[i] = aleatorio;
        cout << "array[" << i << "]=" << arreglo[i] << " ";
    }
    cout << endl;

    // Arreglos de cada metodo
    int *arreglo1 = new int[n];
    int *arreglo2 = new int[n];

    // Se asignan los mismo numeros aleatorios
    arreglo1 = arreglo;
    arreglo2 = arreglo;

    // SELECCION
    // Ocupamos la libreria de chrono para tomar el tiempo
    auto tiempo_inicial = chrono::high_resolution_clock::now();
    // Ordenamos el arreglo con el metodo seleccion
    orden.seleccion(arreglo1, n);
    // Tomamos el tiempo luego de ordenar
    auto tiempo_final = chrono::high_resolution_clock::now();
    // Guardamos el tiempo
    tiempo1 = tiempo_final-tiempo_inicial;

    // QUICKSORT
    // Ocupamos nuevamente la librería chrono
    tiempo_inicial = chrono::high_resolution_clock::now();
    orden.qckSort(arreglo2, n);
    tiempo_final = chrono::high_resolution_clock::now();
    // Guardamos el tiempo
    tiempo2 = tiempo_final-tiempo_inicial;

    // Tabla
    cout << endl;
    cout << " -----------------------------------------" << endl;
    cout << " Método            |Tiempo" << endl;
    cout << " -----------------------------------------" << endl;
    cout << "Seleccion          |" << tiempo1.count() << " milisegundos" << endl;
    cout << "Quicksort          |" << tiempo2.count() << " milisegundos" << endl;
    cout << " -----------------------------------------" << endl;
    cout << endl;

    // Se imprime los arreglos
    if(ver == "s" || ver == "S"){
        imprimirA(arreglo, arreglo1, arreglo2, orden, n);
    }
  }


/* Función ṕrincipal */
int main(int argc, char *argv[]){

  // Variables
  int n;
  string ver;

  // Se comprueba si el numero es entero
   try{
      cout << "Ingrese un número: ";
      cin >> n;

      // Si es numero es menor a 0 no hay nada que ordenar
      if(n == 0){
          cout << "No hay nada que ordenar" << endl;
          return 0;
      }
      // Debe ingresar un numero entre 0 y 1.000.000
      else if (n > 1000001 or n <= 1){
          cout << "Ingrese un valor entre 0 y 1.000.000" << endl;
          cout << "Reinicie el programa" << endl;
          exit(1);
      }
      // Verificar si quiere mostrar el contenido de los vectores
      else{
          cout << "¿Quiere ver el contenido de los vectores? s/n: ";
          cin >> ver;

          // Comprobar si es s o n
          if (ver == "s" || ver == "n"  || ver == "S" || ver == "N"){
              // Si el parámetro es válido se crea el arreglo
              int *arreglo = new int[n];
              llenar(arreglo, n, ver);

          // Si no es S o N se termina el programa
          }else{
              cout << "Inválido, es S o N" << endl;
              cout << "Reinicie el programa" << endl;
              exit(1);
          }
      }
    }
    // Si el numero no es un numero se termina el programa
    catch (const std::invalid_argument& e){
        cout << "No es un numero" << endl;
        cout << "Reinicie el programa" << endl;
        exit(1);
    }
  return 0;
}
