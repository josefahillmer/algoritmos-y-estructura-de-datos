#include <iostream>
using namespace std;

#ifndef ORDENAMIENTO_H
#define ORDENAMIENTO_H

class Ordenamiento {
	private:

	public:
		/* Constructor */
		Ordenamiento();
		//~Ordenamiento();

		/* Metodos */
		void seleccion(int* arreglo, int n);
		void qckSort(int* arreglo, int n);

		// Imprime
		void imprimir(int* arreglo, int n);
};
#endif
