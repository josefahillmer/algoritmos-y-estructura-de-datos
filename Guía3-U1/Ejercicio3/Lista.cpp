#include <iostream>
#include "Lista.h"
using namespace std;

// Constructor
Lista::Lista(){

}

// Método que crea el nodo
void Lista::crear_nodo(int numero){
    Nodo *var_temp = new Nodo;

    // Se crea una variable temporal para ligar el nodo siguienre, creando un espacio de memoria
    var_temp -> numero = numero;
    var_temp -> nodo_siguiente = NULL;

    /* Si es el primero nodo se va a dejar como el primero
    y como el último nodo*/
    if(this->inicio == NULL){
      this-> inicio = var_temp;
      this->ultimo = this->inicio;
    }
    /*Si no es el primer nodo entonces se apuntara al actual último nodo al nuevo
    y dejara el nuevo nodo como el último de la lista*/
    else{
        this->ultimo-> nodo_siguiente = var_temp;
        this->ultimo = var_temp;
    }
    // Se va a ordenar lista
    ordenar_lista(this->inicio);
}

//Método que ordena la lista de menor a mayor
void Lista::ordenar_lista(Nodo *var_temp){
    int aux;
    // Lo recorrera mientras sea ditinto a NULL
    while(var_temp != NULL){
        Nodo *var_temp2 = var_temp-> nodo_siguiente;
        while(var_temp2 !=NULL){
          if(var_temp->numero > var_temp2->numero){
              aux = var_temp2->numero;
              var_temp2->numero = var_temp-> numero;
              var_temp->numero = aux;
          }
          var_temp2 = var_temp2 -> nodo_siguiente;
        }
      var_temp = var_temp-> nodo_siguiente;
    }
}
// Método que imprime la lista ordenada
void Lista::imprimir(){
    // Se utiliza la variable temporal para recorrer la lista
    Nodo *var_temp = this->inicio;

    // La recorre mientras sea distinto a NULL
    while (var_temp != NULL){
        cout << var_temp->numero << " - ";
        var_temp = var_temp-> nodo_siguiente;
    }
    cout << '\n';
}

// Método que retorna el número siguiente
Nodo* Lista::get_inicio(){
    return this-> inicio;
}
