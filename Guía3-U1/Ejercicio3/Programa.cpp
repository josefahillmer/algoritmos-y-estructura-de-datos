#include <iostream>
#include "Lista.h"
using namespace std;


void completar_lista(Lista *lista){
    // Se utiliza el puntero que retorna el inicio de la lista
    Nodo *var_temp = lista-> get_inicio();
    Nodo *var_temp2;
    int numero;

    var_temp2 = var_temp->nodo_siguiente;
    system("clear");
    cout << "Lista rellenada\n" << endl;

    // La recorre mientras sea distinto a NULL
    while(var_temp2 != NULL){
        // Se agrega los numeros que faltan
        if(var_temp2->numero != var_temp->numero + 1){
            numero = var_temp-> numero+1;
            lista-> crear_nodo(numero);
            lista->imprimir();
        }

    // Nodo siguiente
    var_temp = var_temp2;
    var_temp2 = var_temp2->nodo_siguiente;
    }
}

// Menú
void menu(Lista *lista){
    // Variables
    // Se inicia con 1 ya que se debe empezar ingresando un numero
    int op = 1;
    int numero;

    while(op == 1){
        cout << "Ingrese un número: ";
        cin >> numero;

        system("clear");
        // Se crea la lista y se agrega los numeros
        lista-> crear_nodo(numero);
        // Se imprime la lista ingresada
        lista-> imprimir();

        cout << "\n[1] Seguir igresando numeros" << endl;
        cout << "[2] Salir" << endl;
        cout << "Ingrese una opción: ";
        cin >> op;

      }
      // Método que rellenara la lista hasta el mayor número ingresado
      completar_lista(lista);
}

// Función principal
int main(){
    // Se crea las listas
    Lista *lista = new Lista();

    cout << "LISTA ENLAZADA" << endl;
    menu(lista);
    return 0;
}
