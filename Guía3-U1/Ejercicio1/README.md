# Ejercicio 1 "Lista enlazada simple ordenada"

El programa cosiste en la creación de una lista enlazada simple ordenada, se solicitara ingresar números a la lista, para poder salir de la opción de ingresar número debe colocar la opción 2 y se mostrar el estado de la lista ordenada ascendente.
# Empezando

Se debe abrir el archivo por una terminar desde la localizacion de la carpeta correspondiente. En el programa se pedira ingresar un número, luego se mostrara la lista ordenada que se ira actualizando a medida que ingrese números y un menu con 2 opciones las cuales son: seguir ingresando números a la lista y salir. El ingreso de cualquier otra opción tipo letra, número que no sea 1 o 2, se terminara el programa de forma automática.

# Prerequisitos
# Sistema operativo linux:
De preferencia Ubuntu o Debian
# Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h
# Editor de texto
# Make
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

# Ejecutando las pruebas
Para ejecutar el programa se debe realizar los siguientes comandos:
`g++ Programa.cpp Lista.cpp -o Programa`
`make`
`./Programa`

El programa pedira al usuario ingresar un número, se mostrara la lista ordenada la cual se ira actualizando cada vez que se ingrese un número nuevo, debajo de la lista se mostrara un menú con dos opciones:
1. Seguir ingresando números: Esta opción permite ingresar números a la lista enlazada simple, la cual se encuentra arriba de todas las opciones y cada vez que se ingrese un número nuevo se actualizara y ira ordenando los números de menor a mayor.
2. Salir: Cerrara el programa.

Cualquier otra opción tipo letra, números que no sean 1 o 2, números negativos, etc. El programa se cerrara de forma automática.

# Despliegue 
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- C++ : Es un lenguaje de programación
- Librerias: list, iostream

# Autores
- Josefa Hillmer - jhillmer19@alumnos.utalca.cl
