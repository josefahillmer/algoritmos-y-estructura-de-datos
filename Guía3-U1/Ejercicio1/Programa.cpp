#include <iostream>
#include "Lista.h"
using namespace std;

// Función principal
int main(){
    // Se crea la lista
    Lista *lista = new Lista();
    // La opcion es 1, ya que se comienza ingresando un numero
    int op = 1;
    int numero;

    cout << "\n\tLISTA ENLAZADA:" << endl;

    while(op == 1){
        cout << "Ingrese un número: ";
        cin >> numero;
        system("clear");

        // Se agrega los elementos a la Lista
        lista-> crear_nodo(numero);
        // Imprime la lista ORDENADA
        lista-> imprimir();

        cout << "\n[1] Seguir ingresando números" << endl;
        cout << "[2] Salir" << endl;
        cout << "Ingrese una opción: ";
        cin >> op;

    }
    return 0;
}
