#include <iostream>
// Estructura de nodo
#include "Programa.h"
using namespace std;

#ifndef LISTA_H
#define LISTA_H

// Clase lista
class Lista{
    // Atributos privados
    private:
        Nodo *inicio = 0;
        Nodo *ultimo = 0;

    // Atributos públicos
    public:
        Lista();
        void crear_nodo(int numero);
        void ordenar_lista(Nodo *temporal);
        void imprimir();
};
#endif
