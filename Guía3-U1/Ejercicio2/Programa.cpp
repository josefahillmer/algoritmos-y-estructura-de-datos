#include <iostream>
#include "Lista.h"
using namespace std;


void ingresar_numero(Lista *lista){
    int numero;
    int op;

    // Si la opcion es distinta a 2 se hara el ciclo
    while(op != 2){
        cout << "Ingresar un número: ";
        cin >> numero;

        //Se crea y se añade los numeros a la lista
        lista-> crear_nodo(numero);

        cout << "\n[1] Ingresar más números" << endl;
        cout << "[2] Salir" << endl;
        cout << "Ingrese una opcion: ";
        cin >> op;
    }
}

void combinacion(Lista *lista, Lista *lista3){
    int numero;
    // Se utiliza el puntero que retorna el inicio de la lista
    Nodo *var_temp = lista-> get_inicio();

    // La recorre mientras sea distinto a NULL
    while(var_temp != NULL){
        // Se agrega los numeros de la lista a la lista 3
        numero = var_temp-> numero;
        lista3-> crear_nodo(numero);

        // Nodo siguiente
        var_temp = var_temp->nodo_siguiente;

    }
}

// Menú
void menu(Lista *lista1, Lista *lista2, Lista *lista3){
    // Variables
    int op;
    int op2;

    cout << "\nMENÚ\n" << endl;
    cout << "[1] Agregar números" << endl;
    cout << "[2] Ver listas" << endl;
    cout << "[3] Salir" << endl;

    cout << "Ingrese una opción: ";
    cin >> op;

    system("clear");

    if(op == 1){
        cout << "\n[1] Agregar números a la lista 1" << endl;
        cout << "[2] Agregar números a la lista 2" << endl;
        cout << "Ingrese una opción: ";
        cin >> op2;

        if(op2 == 1){
            // Se ingresara numeros a la lista 1
            ingresar_numero(lista1);
            // Se combinara la lista 1 con la lista 3
            combinacion(lista1, lista3);
        }
        else if(op2 == 2){
            // Se ingresara numeros a la lista 2
            ingresar_numero(lista2);
            // Se combinara la lista 2 con la lista 3
            combinacion(lista2, lista3);
        }
        // Se retorna a menú
        menu(lista1, lista2, lista3);
    }
    if(op == 2){
        cout << "Lista ordenada 1: ";
        lista1->imprimir();

        cout << "Lista ordenada 2: ";
        lista2-> imprimir();

        cout << "Lista ordenada combinada: ";
        lista3-> imprimir();

        // Se retorna a menú
        menu(lista1, lista2, lista3);
    }
    if(op == 3){
        cout << "Bye" << endl;
    }
}


// Función principal
int main(){
    // Se crea las listas
    Lista *lista1 = new Lista();
    Lista *lista2 = new Lista();
    Lista *lista3 = new Lista();

    cout << "LISTAS ENLAZADAS COMBINADAS" << endl;
    menu(lista1, lista2, lista3);

}
