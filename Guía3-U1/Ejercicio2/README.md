# Ejercicio 2 "Listas enlazadas simple ordenadas"

El programa cosiste en la creación de 2 listas enlazadas ordenadas y forme una tercera lista que resulte de la mezcla de los elementos de ambas listas. Monstrando el contenido final de las 3 listas. Inicialmente se muestra un menú con 3 opciones, agregar números, ver listas y salir. 

# Empezando

Se debe abrir el archivo por una terminar desde la localizacion de la carpeta correspondiente. En el programa se mostrara un menú principal con tres opciones: agregar números, ver listas y salir. Al ir a la opción de agregar números se mostrara la alternativa de ingresar números a la lista 1 o a la lista 2, y al ingresar el número se podra eleguir entre seguir ingresando numeros o volver al menú principal. La segunda opción se mostrara las 2 listas creadas y la tercera lista que es la combinación de las otras dos.El ingreso de cualquier otra opción tipo letra, número que no sea 1 o 2, se terminara el programa de forma automática.

# Prerequisitos
# Sistema operativo linux:
De preferencia Ubuntu o Debian
# Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h
# Editor de texto
# Make
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

# Ejecutando las pruebas
Para ejecutar el programa se debe realizar los siguientes comandos:
`g++ Programa.cpp Lista.cpp -o Programa`
`make`
`./Programa`

El programa mostrara un menú con tres opciones: 
1. Agregar números: Permite ingresar números a las listas. Esta opción muestra dos nuevas alternativas: 

[1]. Agregar números a la lista 1. 

[2]. Agregar números a la lista 2. 

Al seleccionar cualquiera de las dos listas se pedira ingresar un número, al ingresarlo luego mostrara 2 nuevas opciones: 

[1]. Ingrear más números: Seguir ingresando números a las listas. 

[2]. Salir: Vuelve al menú principal.

2. Ver listas: Mostrara las 2 listas creadas por el usuario que se encontraran ordenadas de menor a mayor y la tercera lista la cual esta combinada por las otras dos e igualmente ordenada de forma ascendente. 

3. Salir: Cerrara el programa.

Cualquier otra opción tipo letra, números que no sean 1 o 2, números negativos, etc. El programa se cerrara de forma automática.

# Despliegue 
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- C++ : Es un lenguaje de programación
- Librerias: list, iostream

# Autores
- Josefa Hillmer - jhillmer19@alumnos.utalca.cl
