# Guía 9 - Unidad 3 "Métodos de Búsqueda - Tablas Hash"
Programa que permite el ingreso y busqueda de información númerica utilizando los siguientes métodos de resolución de colisiones:

1. Reasignación Prueba Lineal (L)
2. Reasignación Prueba Cuadrática (C)
3. Reasignación Doble Dirección Hash (D)
4. Encadenamiento (E)

El método de resolución de colisiones se determinar al momento de ejecutar el programa mediante un parámetro de entrada:
$ ./hash {L|C|D|E}

Por cada ingreso: 
Imprime el contenido del arreglo. Y en caso de ocurrir una colisión, se indicará dónde y cual fue el desplazamiento final.

Por cada búsqueda: Indica la posición donde se encuentra. Y en caso de ocurrir una colisión, se indica dónde y cual fue el desplazamiento final.

# Empezando
 
Se deben abrir los ejercicios por una terminar desde la localizacion de la carpeta correspondiente.
Se pidira ejecutar el programa mediante un parámetro {L|C|D|E} y luego se pedira al usuario ingresar el tamaño del arreglo. Finalmente se mostará un menú con las opciones de: Ingresar dato, buscar dato y salir.

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h

- Editor de texto

- Make:
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

# Ejecutando las pruebas
Para ejecutar el programa se debe realizar los siguientes comandos en la terminal con la carpeta correspondiente:
`g++ Programa.cpp Busqueda.cpp -o hash`

`make`

`./hash + metodo de busqueda {L|C|E|D}`

Se debe inicial el programa mediante un parámetro de entrado: Que puede ser L, C, D, E estos corresponden a los métodos mencionados con anterioridad.
Cuando se seleccione el método se mostratá el método que seleccionó y se pedira al usurio ingresar el tamaño del arreglo que desea almacenar.
Despúes se mostará el siguiente menú:
1. Ingresar dato:
Se pedira ingresar un número, se mostará el estado del arreglo y si existe una colisión se mostará la posición y el desplazamiento que se hizo. 
        Ej:

        Ingrese un número: 43
        Colisión en la posición (6)
        METODO SELECCIONADO: Lineal
        El dato: 43 se desplazada a la posición 8
        [Arreglo] =  { -  -  -  -  -  23  42  5  43  -  66  -  -  -  -  14  -  -  -  - }

2. Buscar dato: 
Se pedira ingresar el dato que desea buscar, si se encuntra el programa mostrará en que posición se encuntra y se imprimirá el arreglo. Si no se encuntra el programa mostratá dato no encontrado.

        Ej: 
        
        Ingrese el dato que desea buscar: 49
        DATO ENCONTRADO
        El dato 49 se encuentra en la posición 12
        [Arreglo] =  { 94  6  1  2  3  23  42  5  43  81  66  28  49  80  51  14  64  15  55  37 }

3. Salir: El programa se cerrara.

Si no ingresa un parámetro de entrada se pedira que reincie el programa.


# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación

# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl