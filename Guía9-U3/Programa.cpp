#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "Busqueda.h"

using namespace std;

//Imprime arreglo
void imprimir(int arreglo[], int largo) {
	cout << "[Arreglo] =  {";
	for (int i = 0; i < largo; ++i) {
		/* code */
		if (arreglo[i] == '\0') {
			cout << " - ";
		} else {
			cout << " " << arreglo[i] << " ";
		}
	}
	cout << "}" << endl;
}

// Procedimiento que busca en caso de colisión
void colision(int *arreglo, int largo, int dato, string metodo, int posicion){
    Busqueda *hash = new Busqueda();

    // Si es lineal
    if (metodo == "L" or metodo == "l") {
      cout << " METODO SELECCIONADO: Lineal" << endl;
      hash->searchLineal(arreglo, largo, dato, posicion);
    }
    // Si es cuadrática
    else if (metodo == "C" or metodo == "c") {
      cout << " METODO SELECCIONADO: Cuadrática" << endl;
      hash->searchCuadratica(arreglo, largo, dato, posicion);
    }
    // Si es doble dirección hash
    else if (metodo == "D" or metodo == "d") {
      cout << " METODO SELECCIONADO: Doble Dirección Hash" << endl;
      hash->searchDobleHash(arreglo, largo, dato, posicion);
    }
    // Si es encadenamiento
    else if (metodo == "E" or metodo == "e") {
      cout << " METODO SELECCIONADO: Encadenamiento" << endl;
      hash->searchEncadenamiento(arreglo, largo, dato, posicion);
    }
    // Se imprime el arreglo
    imprimir(arreglo, largo);
}

// Procedimientoque comprueba el metodo para ingresar dato en caso de colision
void ingreso(int *arreglo, int largo, int dato, string metodo, int posicion) {

	Busqueda *hash = new Busqueda();

  // Si el método es lineal
	if (metodo == "L" or metodo == "l") {
		cout << " METODO SELECCIONADO: Lineal" << endl;
		hash->ingresoLineal(arreglo, largo, dato, posicion);

	}
  // Si el método es cuadratica
  else if (metodo == "C" or metodo == "c") {
		cout << " METODO SELECCIONADO: Cuadrática" << endl;
		hash->ingresoCuadratica(arreglo, largo, dato, posicion);

	}
  // Si el método es doble dirección hash
  else if (metodo == "D" or metodo == "d") {
		cout << " METODO SELECCIONADO: Doble Dirección Hash" << endl;
		hash->ingresoDobleHash(arreglo, largo, dato, posicion);

	}
  // Si el método es encadenamiento
  else if (metodo == "E" or metodo == "e") {
		cout << " METODO SELECCIONADO: Encadenamiento" << endl;
		hash->searchEncadenamiento(arreglo, largo, dato, posicion);
	}
  // Se imprime
	imprimir(arreglo, largo);
}

// Procedimiento que revisa el estado del arreglo
bool estado(int arreglo[], int largo){
  // Variable
  int cont = 0;
  // Recorre el arreglo
  for (int i = 0; i < largo; ++i){
    if (arreglo[i] == '\0') {
      /* continuar */
      cont ++;
    }
  }
  // Se encuntra vacio
  if (cont == largo) {
    return true;
  }
  // NO esta vacio
  else {
    return false;
  }
}

// Procedimiento que llena el arreglo con 0
void llenarArreglo(int *arreglo, int largo){
    // Se llena el arreglo con 0
    for(int i=0; i<largo; i++){
        arreglo[i] = '\0';
    }
}

// Menú
void menu(string metodo, int *arreglo, int largo){
    // Variables
    int opcion;
    int dato;
    Busqueda *hash = new Busqueda();
    int posicion;

    // Menú
    do{
      cout << "\n\t  .:MENU:." << endl;
      cout << " 1. Ingresar dato" << endl;
      cout << " 2. Buscar dato" << endl;
      cout << " 3. Salir" << endl;
      cout << "Ingrese su opción: ";
      cin >> opcion;

      switch(opcion){
          // Insertar
          case 1:
              system("clear");
              cout << "Ingrese un número: ";
              cin >> dato;
              // Se obtiene la posición con hash
              posicion = hash->hash(dato, largo);

              //Se agrega si la posicion se encuntra vacia
              if(arreglo[posicion] == '\0'){
                  arreglo[posicion] = dato;
                  imprimir(arreglo, largo);
              }
              // Si no esta vacia, se indica donde ocurre la colision
              else{
                  cout << "Colisión en la posición (";
                  cout << posicion << ")" << endl;
                  ingreso(arreglo, largo, dato, metodo, posicion);
              }
              break;

          // Busqueda
          case 2:
              system("clear");
              cout << "Ingrese el dato que desea buscar: ";
              cin >> dato;

              //Si el arreglo no se encuntra vacío se busca el dato
              if(estado(arreglo, largo) == false){
                  // se obtiene la poción
                  posicion = hash->hash(dato, largo);
                  // Si se encuentra el dato, se imprime la posicion
                  if(arreglo[posicion] == dato){
                      cout << "DATO ENCONTRADO" << endl;
                      cout << "El dato " << dato << " se encuentra en la posición";
                      cout << posicion << endl;
                      // Se imprime el arreglo
                      imprimir(arreglo, largo);
                  }
                  // Si hay colisión
                  else{
                      cout << "Colisión en la posición: " << posicion << endl;
                      // se busca en caso de colision
                      colision(arreglo, largo, dato, metodo, posicion);
                  }
                }
                // Se encuntra vacío
                else{
                    cout << "Arreglo vacío" << endl;
                    cout << "Elemento: " << dato << " no encontrado" << endl;
                }
                break;

            // Salir
            case 3:
                exit(1);
                break;

            // Opcion no valida y retorno al menu
            default:
                cout << "Opción no valida" << endl;
                menu(metodo, arreglo, largo);
                break;
        }
      }
      // Si ingresa algo distinto a 3
      while (opcion != 3);
}

// Función principal o main
int main(int argc, char *argv[]){
    // Variables
    int largo;

    //Se valida el ingreso de argumento
    if(argc != 2){
        // NO ingresa dos argumentos
        cout << "ERROR. Argumento inválido" << endl;
        cout << "El segundo argumento debe ser de tipo string L|C|D|E" << endl;
        cout << "Reinicie el programa" << endl;
        exit(1);
    }
    // Si ingresa los dos argumentos
    else{
        // Comprobación
        try{
            // Segundo argumento
            string metodo = argv[1];

            // Si es lineal
            if (metodo == "L" or metodo == "l") {
              cout << "MÉTODO SELECCIONADO: Lineal" << endl;
            }
            // Si es Cuadrática
            else if (metodo == "C" or metodo == "c"){
              cout << "MÉTODO SELECCIONADO: Cuadrática" << endl;
            }
            // Si es doble dirección hash
            else if (metodo == "D" or metodo == "d"){
              cout << "MÉTODO SELECCIONADO: Doble Dirección Hash" << endl;
            }
            // Si es encadenamiento
            else if (metodo == "E" or metodo == "e"){
              cout << "MÉTODO SELECCIONADO: Encadenamiento" << endl;
            }
            // Ningúno de los anteriores
            else{
              // Argumento invalido
              cout << "ERROR. Argumento inválido" << endl;
              cout << "Ingrese la letra correspondiente al método";
              cout << "Opciones posibles: L|C|D|E" << endl;
              cout << "Reinicie el programa" << endl;
              exit(1);
            }

            cout << "Ingrese el tamaño del arreglo: ";
            cin >> largo;
            // Se inicializa el arreglo
            int arreglo[largo];
            // Se llena el arreglo con 0
            llenarArreglo(arreglo, largo);

            // Se llama menú
            menu(metodo, arreglo, largo);
        }
        // Argumento invalido
        catch (const std::invalid_argument& e){
        // segundo argumento no es una Letra
        cout << "ERROR. Argumento inválido" << endl;
        cout << "Segundo argumento debe ser de tipo string L|C|D|E" << endl;
        cout << "Reinicie el programa" << endl;
        exit(1);
      }
    }
    return 0;
}
