#include <iostream>
#include <fstream>
#include "Busqueda.h"

// Constructor
Busqueda::Busqueda(){

}

// Procemiento que genera el arreglo para el metodo encadenamiento
void Busqueda::arregloEncadenamiento(Nodo **arreglo, int *arreglo2, int largo){
    // Desplazamiento
    for(int i=0; i<largo; i++){
        arreglo[i] -> dato = arreglo2[i];
        arreglo[i] -> sig = NULL;
    }
}

// Procemiento que busca por el metodo encadenamiento
void Busqueda::searchEncadenamiento(int *arreglo, int largo, int dato, int posicion){
    // Se generan nodos auxiliares que ayudan a buscar
    Nodo *tmp = NULL;
    Nodo *arregloaux[largo];

    for(int i=0; i<largo; i++){
        arregloaux[i] = new Nodo();
        arregloaux[i]-> sig = NULL;
        arregloaux[i]->dato = '\0';
    }
    arregloEncadenamiento(arregloaux, arreglo, largo);

    // Si el dato ya se encuentra en el arreglo
    if((arreglo[posicion] != '\0') && (arreglo[posicion] == dato)){
        cout << "El dato: " << dato << " se encuentra en la posición " << posicion << endl;
    }
    // Se busca
    else{
        tmp = arregloaux[posicion]->sig;
        // temp tiene que ser distitno a null y distinto al dato para cambiar la posicion
        while((tmp != NULL) && (tmp->dato != dato)){
            tmp = tmp->sig;
        }
        // Si tmp es igual a null no hay nada que mostrar
        if(tmp == NULL){
            cout << "El dato: " << dato << " no se encuentra" << endl;
        }
        // Si no se indica la posicion en la que se encuntra
        else{
            cout << "El dato: " << dato << " se encuntra en la posición " << posicion << endl;
        }
    }
}

// Procedimiento que busca con el metodo doble hash
void Busqueda::searchDobleHash(int *arreglo, int largo, int dato, int posicion){
		int nuevaPosicion;

    // Si el dato ya se encuntra en el arreglo
	  if ((arreglo[posicion] != '\0') && (arreglo[posicion] == dato)){
		     cout << "El dato: " << dato << " se encuntra en la posición " << posicion << endl;
	  }
    // Si no se encuntra
    else{
		    nuevaPosicion = ((posicion + 1)%largo - 1) + 1;
        //Revisa si la nueva posición esta vacia
		    while ((nuevaPosicion <= largo) && (arreglo[nuevaPosicion] != '\0') && (nuevaPosicion != posicion) && (arreglo[nuevaPosicion] != dato)) {
			      //Resta uno al tamaño porque el arreglo parte desde cero
			      nuevaPosicion = ((nuevaPosicion + 1)%largo -1) + 1;
		}
    // SI el arreglo esta vacio
		if((arreglo[nuevaPosicion] == '\0') || (nuevaPosicion == posicion)) {
			 cout << "El dato: " << dato << " no se encuntra" << endl;
		}
    // Se indica la posicion
    else{
			 cout << "El dato: " << dato << " se encuntra en la posición " << posicion << endl;
		}
	}
}

// Procemiento que ingresa con el metodo doble hash
void Busqueda::ingresoDobleHash(int *arreglo, int largo, int dato, int posicion){
	int nuevaPosicion;

  // Revisa si el dato ya existe en el arreglo
	if ((arreglo[posicion] != '\0') && (arreglo[posicion] == dato)) {
		  cout << "El dato: " << dato << " se encuentra en la posición " << posicion << endl;
	}
  // Si no existe
  else {
		//Se le resta uno al largo porque el arreglo parte desde cero
		nuevaPosicion = ((posicion + 1)%largo - 1) + 1;
    //Revisa si la nueva posición esta vacia
		while ((nuevaPosicion <= largo) && (arreglo[nuevaPosicion] != '\0') && (nuevaPosicion != posicion) && (arreglo[nuevaPosicion] != dato)){
      //Resta uno al tamaño porque el arreglo parte desde cero
      nuevaPosicion = ((nuevaPosicion + 1)%largo - 1) + 1;
		}
    // Si el arreglo esta vacio en la posicion
		if (arreglo[nuevaPosicion] == '\0' or (arreglo[nuevaPosicion] != dato) ) {
			// Revisa si la nueva posición esta vacia
			arreglo[nuevaPosicion] = dato;
      //  Si el arreglo esta lleno
			if(nuevaPosicion == largo + 1){
				cout << " Arreglo Lleno" << endl;
			}
      // Queda aun espacacio y se indica el desplazamiento
      else{
				cout << "El dato: " << dato << " se desplazada a la posición: " << nuevaPosicion << endl;
			}
		}
    // Si no esta vacio se indica la posicion en la que se encuentra
    else {
				cout << "El dato: " << dato << " se encuentra en la posición " << posicion << endl;
		}
	}
}

// Procedimiento que busca con el método cuadratica
void Busqueda::searchCuadratica(int *arreglo, int largo, int dato, int posicion){
	int nuevaPosicion;
	int i;

  // Revisa si el dato ya existe en el arreglo
	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == dato)) {
		cout << "El dato: " << dato << " se encuntra en la posición " << posicion << endl;
	}
  else {
		i = 1;
		// Avanza a la nueva posición con un incremente al cuadrado
		nuevaPosicion = (posicion + (i*i));
    // Revisa si la nueva posición esta vacia
		while ( (arreglo[nuevaPosicion] != '\0') && (arreglo[nuevaPosicion] != dato)) {
      //Avanza una posición
			i = i + 1;
      //Genera una nueva posición
			nuevaPosicion = (posicion + (i*i));

      // Si la nueva posicion es más grande que el largo del arreglo se vuelve al comienzo
			if(nuevaPosicion > largo){
				i = 1;
        //Vuelve al inicio del arreglo
				nuevaPosicion = 0;
				posicion = 0;
			}
		}
    // Si el arreglo esta vacio
		if (arreglo[nuevaPosicion] != '\0')	{
			cout << "El dato: " << dato << " no se encuentra" << endl;
		}
    // Se indica la posicion
    else {
			cout << "El dato: " << dato << " se encuentra en la posición " << posicion << endl;
		}
	}
}

// Se ingresa datos con el método cuadrática
void Busqueda::ingresoCuadratica(int *arreglo, int largo, int dato, int posicion){
	int nuevaPosicion;
  // Variable que aumentara cuadráticamente
	int i;

  // Se revisa si el dato ya existe
	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == dato) ) {
		cout << "El dato: " << dato << " se encuntra en la posición " << posicion << endl;
	}
  // Si no existe se agrega
  else {
		i = 0;
		// Avanza a la nueva posición con un incremente al cuadrado
		nuevaPosicion = (posicion + (i*i));
    //Revisa si la nueva posición esta vacia
		while( (arreglo[nuevaPosicion] != '\0') && (arreglo[nuevaPosicion] != dato) ) {
			//Aumenta para avanzar una posición
			i++;
      //Genera una nueva posición
			nuevaPosicion = (posicion + (i*i));

      // Si la nueva posicion es más grande que el largo del arreglo se vuelve al comienzo
			if (nuevaPosicion > largo) {
				i = 0;
        //Vuelve al inicio del arreglo
				nuevaPosicion = 0;
				posicion = 0;
			}
		}
    // Si el arreglo esta vacio, hay espacio
		if (arreglo[nuevaPosicion] == '\0') {
			/* disponible */
			arreglo[nuevaPosicion] = dato;
			cout << "El dato: " << dato << " se desplazada a la posición " << nuevaPosicion << endl;
		}
    // Se indica la posicion
    else {
			cout << "El dato: " << dato << " se encuentra en la posición " << posicion << endl;
		}
	}
}

// Procedimiento que busca con el método lineal
void Busqueda::searchLineal(int *arreglo, int largo, int dato, int posicion){
	int nuevaPosicion;

  // Revisa si el dato ya existe
	if ((arreglo[posicion] != '\0') && (arreglo[posicion] == dato) ) {
		cout << "El dato: " << dato << " se encuntra en la posición " << posicion << endl;
	}
  // Si no existe
  else{
    // Se genera una nueva posicion
		nuevaPosicion = posicion + 1;
    //Revisa si la nueva posición esta vacia
		while ((arreglo[nuevaPosicion] != '\0') && (nuevaPosicion <= largo) && (nuevaPosicion != posicion) && (arreglo[nuevaPosicion] != dato)){
			nuevaPosicion = nuevaPosicion + 1;

      //  Si el arreglo esta lleno
			if (nuevaPosicion == largo + 1) {
         //Vuelve al inicio
				nuevaPosicion = 0;
			}
		}
    // Si el arreglo esta vacio
		if( (arreglo[nuevaPosicion] == '\0') or (nuevaPosicion == posicion)) {
			cout << "El dato: " << dato << " no se encuentra" << endl;
		}
    // Se imprime la posicion
    else {
			cout << "El dato: " << dato << " se encuentra en la posición " << posicion << endl;
		}
	}
}

// Procedimiento que ingresa con el método lineal
void Busqueda::ingresoLineal(int *arreglo, int largo, int dato, int posicion){
	int nuevaPosicion;
	int cont;

  // Revisa si el dato ya existe
	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == dato)) {
		cout << "El dato: " << dato << " se encuntra en la posición " << posicion << endl;
	}
  // Si no existe se ingresa
  else {
		cont = 0;
    // Se genera una nueva posicion
		nuevaPosicion = posicion + 1;

    //Revisa si la nueva posición esta vacia
		while ((nuevaPosicion != posicion)  && (arreglo[nuevaPosicion] != '\0') && (nuevaPosicion <= largo) && (arreglo[nuevaPosicion] != dato) ){
			nuevaPosicion = nuevaPosicion + 1;

      // Si el arreglo esta lleno
			if (nuevaPosicion == (largo + 1)){
        // Vuelve al inicio
				nuevaPosicion = 0;
			}
			cont++;
		}

    // Si esta vacio, hay espacio
		if (arreglo[nuevaPosicion] == '\0')	{
			// Posicion libre
			arreglo[nuevaPosicion] = dato;
			cout << "El dato: " << dato << " se desplazada a la posición " << nuevaPosicion << endl;
		}
    // Se indica la posicion
    else {
			cout << "El dato: " << dato << " se encuentra en la posición " << posicion << endl;
		}
    // El arreglo esta lleno
		if (cont == largo) {
			cout << " Arreglo Lleno" << endl;
		}
	}
}

// Procedimiento que asigna una posicion para guardar el dato
int Busqueda::hash(int dato, int largo) {
	// Función hash por módulo
	dato = (dato%(largo - 1)) + 1;
	return dato;
}
