#include <iostream>
using namespace std;

#ifndef BUSQUEDA_H
#define BUSQUEDA_H

//Estructura del nodo
typedef struct _Nodo{
	int dato;
	struct _Nodo *sig;
} Nodo;

// Clase
class Busqueda{
	// Metodos privados
	private:

	// Metodos publicos
	public:
		//Constructor
		Busqueda();
		~Busqueda();

		// Metodos de ingreso
		int hash(int clave, int largo);
		void ingresoLineal(int *arreglo, int largo, int clave, int posicion);
		void ingresoCuadratica(int *arreglo, int largo, int clave, int posicion);
		void ingresoDobleHash(int *arreglo, int largo, int clave, int posicion);
		void arregloEncadenamiento(Nodo **arreglo, int *arreglo2, int largo);

		// Metodos de busqueda
		void searchLineal(int *arreglo, int largo, int clave, int posicion);
		void searchCuadratica(int *arreglo, int largo, int clave, int posicion);
		void searchDobleHash(int *arreglo, int largo, int clave, int posicion);
		void searchEncadenamiento(int *arreglo, int largo, int clave, int posicion);

};
#endif
