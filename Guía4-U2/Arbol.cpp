#include <iostream>
#include "Arbol.h"

using namespace std;

Arbol::Arbol(){
}

// Procedimiento que crea un nuevo Nodo
Nodo* Arbol::crear_Nodo(int numero, Nodo *padre){
    Nodo *tmp;
    tmp = new Nodo;

    // Se le asigna un valor al Nodo
    tmp ->dato = numero;
    // Se crea los subarboles de izquiera y derecha
    tmp->izq = NULL;
    tmp->der = NULL;
    tmp->Nodo_padre = padre;

    return tmp;
}

// Procedimiento que inserta el Nodo donde corresponde
void Arbol::insertar(Nodo *&raiz, int numero, Nodo *padre){
    int valor_raiz;
    // Si la raiz no tiene elementos, la raiz sera tmp
    if(raiz == NULL){
        Nodo *tmp;
        tmp = crear_Nodo(numero, padre);
        raiz = tmp;
        insertar(raiz, numero, padre);
    }
    // Se compara el valor de la raiz con el que se quiere ingresar
    else{
        valor_raiz = raiz->dato;
        // Si el numero ingresado es menor a la raiz se ingresa al lado izquierdo
        if(numero < valor_raiz){
            insertar(raiz->izq, numero, raiz);
        }
        // Si el número ingresado es mayor a la raiz se ingresa por el lado derecho
        if(numero > valor_raiz){
            insertar(raiz->der, numero, raiz);
        }
    }
}

// Procedimineto para buscar un elemento en el árbol
bool Arbol::busqueda(Nodo *raiz, int numero){
    int valor_raiz;
    valor_raiz = raiz->dato;
    // El arbol se encuentra vacío
    if(raiz == NULL){
        return false;
    }
    //Si la busqueda es igual al Nodo
    else if(valor_raiz == numero){
        return true;
    }
    // Si la busqueda es menor al Nodo, se ve el lado izquierdo
    else if(numero < valor_raiz){
        return busqueda(raiz->izq, numero);
    }
    // Si la bsuqueda es mayor al Nodo, se ve el lado derecho
    else{
        return busqueda(raiz->der, numero);
    }
}

// Procedmineto que busca el Nodo más a la izquiera
Nodo* Arbol::encontrar_mas_izquierdo(Nodo *raiz){
    if(raiz->izq){
        // Retorna el Nodo con el valor más pequeño
        return encontrar_mas_izquierdo(raiz->izq);
    }else{
        return raiz;
    }
}

// Se reemplaza el Nodo que se quiere eliminar por su hijo
void Arbol::reemplazar_hijo(Nodo *raiz, Nodo *cambio){
    if(raiz->Nodo_padre){
        // cambio por hijo izquierdo
        if(raiz->dato == raiz->Nodo_padre->izq->dato){
            raiz->Nodo_padre->izq = cambio;
        }
        // cambio por hijo derecho
        else if(raiz->dato == raiz->Nodo_padre->der->dato){
            raiz->Nodo_padre->der = cambio;
        }
    }
    // Cambia el padre del nuevo Nodo por el del Nodo que se elimino
    if(cambio){
        cambio->Nodo_padre = raiz->Nodo_padre;
    }
}

// Procedimiento que eliminar el Nodo
void Arbol::eliminar_Nodo(Nodo *&Nodo){
    // Si el Nodo tiene dos hijos
    if(Nodo->izq && Nodo->der){
        /* Se buscara el Nodo más izquierdo para reemplazarlo en la
        posición del que se eliminara*/
        Nodo *menor = encontrar_mas_izquierdo(Nodo->der);
        Nodo->dato = menor->dato;
        eliminar_Nodo(menor);
    }
    // Si tiene un hijo en el lado izquierdo
    else if(Nodo->izq){
        reemplazar_hijo(Nodo, Nodo->izq);
        Nodo->izq = NULL;
    }
    // Si tiene un hijo en el lado derecho
    else if(Nodo->der){
        reemplazar_hijo(Nodo, Nodo->der);
        Nodo->der = NULL;
    }
    // Si no tiene hijos
    else{
        reemplazar_hijo(Nodo, NULL);
    }
}

// Procedimiento que elimina el Nodo del árbol
void Arbol::elimina(Nodo *&raiz, int numero){
    // Si el número es menor
    if(numero < raiz->dato){
        elimina(raiz->izq, numero);
    }
    // Si el número es mayor
    else if(numero > raiz->dato){
        elimina(raiz->der, numero);
    }
    // Si es un Nodo se va al procedimiento eliminar Nodo
    else{
        eliminar_Nodo(raiz);
    }
}

// Procedmineto que muestra el arbol en inorden
void Arbol::mostrar_arbol_inorden(Nodo *raiz){
    /* Si la raiz es distina a NULL entonces se recorre primero hijo izquierdo,
    luego el padre y finalmente el hijo derecho*/
    if(raiz != NULL){
        mostrar_arbol_inorden(raiz->izq);
        cout << raiz->dato << " - ";
        mostrar_arbol_inorden(raiz->der);
    }
}

// Procedimiento que muestra el árbol en preorden
void Arbol::mostrar_arbol_preorden(Nodo *raiz){
    /* Si la raiz no es NULL, entonces se recorre en el orden previo, es decir
    primero el padre, luego el hijo izquierdo y finalmente el hijo derecho. */
    if(raiz != NULL){
        cout << raiz->dato << " - ";
        mostrar_arbol_preorden(raiz->izq);
        mostrar_arbol_preorden(raiz->der);
    }
}
// Procedmineto que muestra el árbol en posorden
void Arbol::mostrar_arbol_posorden(Nodo *raiz){
    /* Si la raiz no es NULL, entonces se recorre en el orden posterior, es decir
    primero hijo izquierdo, luego el hijo derecho y finalmente el padre */
    if(raiz != NULL){
        mostrar_arbol_posorden(raiz->izq);
        mostrar_arbol_posorden(raiz->der);
        cout << raiz->dato << " - ";
    }
}
