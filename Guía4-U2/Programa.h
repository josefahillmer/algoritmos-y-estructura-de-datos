typedef struct _Nodo {
  int dato;
  struct _Nodo *izq;
  struct _Nodo *der;
  struct _Nodo *nodo_padre;
} Nodo;
