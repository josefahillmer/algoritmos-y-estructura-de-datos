# Guía 4 - Unidad 2 "Árboles Binarios de Búsqueda"
El programa cosiste en la creación de un árbol binario de búsqueda solo con números, donde los los elementos son únicos, es decir, no hay repetición de números. El algortimo tendra las siguientes opciones: ingresar número, mostrar árbol (se podra ordenar de 3 formas distintas: preorden, inorden y posorden), eliminar nodo, modificar nodo, mostrar grafo, salir.

# Empezando
Se deben abrir los ejercicios por una terminar desde la localizacion de la carpeta correspondiente. 
Se mostrara un menú con 6 opciones: ingresar número, mostrar árbol, eliminar nodo, modificar nodo, mostrar grafo, salir. En la opción mostrar el contenido del árbol se podra ordenar de 3 formas distintas: preorden, inorden y posorden.

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp y .h

- Editor de texto

- Make:
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:
`sudo apt install make`
Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

- Se debe tener instalado el paquete:  graphviz

Para instalar en Debian, como usuario root ejecutar:
`sudo apt-get install graphviz`

- Para generar la imagen a partir del archivo fuente datos.txt. 
En la línea de comandos ejecutar:
`$ dot -Tpng -ografo.png datos.txt`
Generará el archivo con la imagem grafo.png

Para visualizar la imagen, se puede ejecutar lo siguiente:
`$ eog grafo.png`

# Ejecutando las pruebas
Para ejecutar el programa se debe realizar los siguientes comandos en la terminal con la carpeta correspondiente:
`g++ Programa.cpp Arbol.cpp -o Programa`

`make`

`./Programa`

El programa mostrara un menú con las siguientes opciones:

**1. Ingresar número:** Se solicitara el elemento que quiere ingresar al árbol. Pero solo se agregará números enteros positivos, si ingres una letra o un número negativo el programa volvera al menú principal.

**2. Mostrar árbol:** Se pedira al usuario ingresar el tipo de recorrido que quiere mostrar, con un menú con las siguientes opciones:

    1. Preorden: Se se recorre en el orden previo, es decir, primero el padre, luego el hijo izquierdo y finalmente el hijo derecho.

    2. Inorden: Se recorre primero hijo izquierdo, luego el padre y finalmente el hijo derecho.

    3. Posorden: Se recorre en el orden posterior, es decir, primero hijo izquierdo, luego el hijo derecho y finalmente el padre

    4. Volver al menú principal
Al seleccionar alguna opción de orden se mostrara el árbol en la parte superior del menú. Si ingresa otra opción de recorrido se ira actualizara el árbol.

Ejemplo: Para el ingreso de los siguientes valores: 120, 87, 140, 43, 99, 130, 22, 65, 93, 135, 56.
Los resultados de los diferentes recorridos serı́an:

        — Preorden—
        120 - 87 - 43 - 22 - 65 - 56 - 99 - 93 - 140 - 130 - 135
        — Inorden—
        22 - 43 - 56 - 65 - 87 - 93 - 99 - 120 - 130 - 135 - 140
        — Posorden—
        22 - 56 - 65 - 43 - 93 - 99 - 87 - 135 - 130 - 140 - 120
        

**3. Eliminar nodo:** Se pedira al usuario el nodo que desea eliminar. Lo eliminara dependiendo si es un nodo sin hijos, un nodo con un hijo o un nodo con dos hijos. 

**4. Modificar nodo:** Se pedira al usuario eliminar el elemento que quiere modificar y luego insertar un nuevo elemento. 

**5. Monstrar grafo:** Se abrira un archivo llamado grafo.png donde se monstrara el árbol correspondiente. 

**6. Salir:** Salir del programa.

# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación

# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl