#include <iostream>
// Definición del nodo
#include "Programa.h"

#ifndef ARBOL_H
#define ARBOL_H

class Arbol{
    private:

    public:
        Arbol();
        Nodo* crear_nodo(int numero, Nodo *padre);
        Nodo* encontrar_mas_izquierdo(Nodo *raiz);
        bool busqueda(Nodo *raiz, int numero);
        void destruir_nodo(Nodo *nodo);
        void insertar(Nodo *&raiz, int numero, Nodo *padre);
        void reemplazar_hijo(Nodo *raiz, Nodo *cambio);
        void eliminar_nodo(Nodo *&nodo_eliminado);
        void elimina(Nodo *&raiz, int numero);
        void mostrar_arbol_inorden(Nodo *raiz);
        void mostrar_arbol_preorden(Nodo *raiz);
        void mostrar_arbol_posorden(Nodo *raiz);

};
#endif
