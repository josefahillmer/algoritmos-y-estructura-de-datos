#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include "Arbol.h"

using namespace std;

/* para usar fork() */
#include <unistd.h>

// Procedimineto que hace el grafo
class Graficar {
    private:
        Nodo *arbol = NULL;

    public:
        // Constructor de la Clase Graficar.
        Graficar(Nodo *raiz) {
	        this->arbol = raiz;
        }

        // ofstream es el tipo de dato correspondiente a archivos en cpp (el llamado es ofstream &nombre_archivo).
        void recorrerArbol(Nodo *p, ofstream &archivo) {
	        string infoTmp;
	        /* Se enlazan los Nodos del grafo, para diferencia entre izq y der a cada Nodo se le entrega un identificador*/
	        if (p != NULL) {
	        // Por cada Nodo ya sea por izq o der se escribe dentro de la instancia del archivo.
	        if (p->izq != NULL) {
		        archivo<< p->dato << "->" << p->izq->dato << ";" << endl;
	        }

	        else {
		        infoTmp = to_string(p->dato) + "i";
		        infoTmp = "\"" + infoTmp + "\"";
		        archivo << infoTmp << "[shape=point]" << endl;
		        archivo << p->dato << "->" << infoTmp << ";" << endl;
	        }

	        infoTmp = p->dato;

	        if (p->der != NULL) {
		        archivo << p->dato << "->" << p->der->dato << ";" << endl;
	        }

	        else {
		        infoTmp = to_string(p->dato) + "d";
		        infoTmp = "\"" + infoTmp + "\"";
		        archivo << infoTmp << "[shape=point]" << endl;
		        archivo << p->dato << "->" << infoTmp << ";" << endl;
	        }

	        // Se realizan los llamados tanto por la izquierda como por la derecha para la creación del grafo.
	        recorrerArbol(p->izq, archivo);
	        recorrerArbol(p->der, archivo);
	        }

	        return;
        }

        void grafico() {
	        ofstream archivo;
	        // Se abre/crea el archivo datos.txt, a partir de este se generará el grafo.
	        archivo.open("datos.txt");
	        // Se escribe dentro del archivo datos.txt "digraph G { ".
	        archivo << "digraph G {" << endl;
	        // Se pueden cambiar los colores que representarán a los Nodos, para el ejemplo el color será verde.
	        archivo << "node [style=filled fillcolor=cyan];" << endl;
	        // Llamado a la función recursiva que genera el archivo de texto para creación del grafo.
	        recorrerArbol(this->arbol, archivo);
	        // Se termina de escribir dentro del archivo datos.txt.
	        archivo << "}" << endl;
	        archivo.close();

	        // Genera el grafo
	        system("dot -Tpng -ografo.png datos.txt &");
          // Visualizar el grafo
	        system("eog grafo.png &");
        }
};

// Procedimiento que valida si el dato ingresado es un numero
bool validar_numero(string numero){
    bool valido = true;
    for(int i=0; i<numero.size(); i++){
        // se verifica si es un numero con el código ascci
        if(numero[i] < 48 || numero[i] > 57){
            valido = false;
        }
    }
    return valido;
}

// Procedimiento que mostrara el arbol segun el tipo de recorrido
void recorrido(Arbol *nuevo_arbol, Nodo *raiz){
    int op;
    system("clear");

    do{
        cout << "\n\n\tTipos de recorrido" << endl;
        cout << " [1] Preorden" << endl;
        cout << " [2] Inorden" << endl;
        cout << " [3] Posorden" << endl;
        cout << " [4] Volver al menú principal" << endl;

        cout <<"\n Ingrese una opción: ";
        cin >> op;

        // Si raiz es ditinta a NULL se puede mostrar el árbol
        if(raiz != NULL){
            // Preoden
            if(op == 1){
                system("clear");
                cout << "\n Árbol en preorden" << endl;
                nuevo_arbol-> mostrar_arbol_preorden(raiz);
            }
            // Inorden
            else if(op == 2){
                system("clear");
                cout << "\n Árbol en inorden" << endl;
                nuevo_arbol-> mostrar_arbol_inorden(raiz);
            }
            // Posorden
            else if(op == 3){
                system("clear");
                cout << "\n Árbol en posorden" << endl;
                nuevo_arbol-> mostrar_arbol_posorden(raiz);
            }
        }
        // Si no, está vacio
        else{
            cout << "El árbol se encuntra vacio" << endl;
        }
    }
    while(op!=4);
}

// Procedimiento que modifica un elemento del árbol
void modificar(Arbol *nuevo_arbol, Nodo *raiz){
  int numero;
  int modificado;

  cout << "Ingrese el elemento que desea modificar: ";
  cin >> numero;
  // El elemento fue encontrado
  if(nuevo_arbol-> busqueda(raiz, numero) == true){
      cout << "Elemento " << numero << " localizado" << endl;
      // Se elimina el Nodo
      nuevo_arbol-> elimina(raiz, numero);
      cout << "Ingrese nuevo elemento: ";
      cin >> modificado;
      // Se inserta el nuevo Nodo
      nuevo_arbol-> insertar(raiz, modificado, NULL);
  }
}

// Procedmineto que elimina un elemento del arbol
void eliminar(Arbol *nuevo_arbol, Nodo *raiz){
    bool validar;
    string numero_eliminado;

    cout << "Ingrese el número que desea eliminar: ";
    cin.ignore();
    getline(cin, numero_eliminado);

    // Se valida que sea un numero
    validar = validar_numero(numero_eliminado);

    // Si es un numero se elimina
    if(validar = true){
        nuevo_arbol-> elimina(raiz, stoi(numero_eliminado));
    }else{
        cout << numero_eliminado << " No es un número" << endl;
        sleep(1);
    }

}

// Procedimineto que ingresar un número al árbol
void ingresar(Arbol *nuevo_arbol, Nodo *&raiz){
    // Variables
    bool validar;
    string numero;
    Nodo *Nodo = NULL;

    system("clear");
    cout << "Ingrese un número: ";
    // Para descartar el salto de línea que se ha quedado en el buffer de entrada
    cin.ignore();
    getline(cin, numero);

    // Para verificar si se ingreso un numero
    validar = validar_numero(numero);

    if(validar == true){
        // se crea el Nodo
        Nodo = nuevo_arbol->crear_Nodo(stoi(numero), NULL);
        // se inicializa la raiz del árbol
        if(raiz == NULL){
            raiz = Nodo;
        }
        // Si hay más de un Nodo
        else{
            nuevo_arbol-> insertar(raiz, stoi(numero), NULL);
        }
    }
    // Si no es un numero o si es negativo
    else{
        cout << numero << " No es un número" << endl;
        sleep(1);
    }
  }


// Menú
void menu(Arbol *nuevo_arbol, Nodo *raiz){
    int op;

    do{

        cout << "\nMENÚ\n" << endl;
        cout << " [1] Ingresar número" << endl;
        cout << " [2] Mostrar árbol" << endl;
        cout << " [3] Eliminar Nodo" << endl;
        cout << " [4] Modificar Nodo" << endl;
        cout << " [5] Mostrar grafo" << endl;
        cout << " [6] Salir" << endl;

        cout << "Ingrese opción: ";
        cin >> op;

        switch(op){
          case 1:
              // Se crea el Nodo y se ingresa un dato
              ingresar(nuevo_arbol, raiz);
              break;

          case 2:
              // Se ven los distintos recorridos que tiene el árbol
              recorrido(nuevo_arbol, raiz);
              break;

          case 3:
              // Se elimina un Nodo (dato)
              eliminar(nuevo_arbol, raiz);
              break;

          case 4:
              // Se modificara un Nodo
              modificar(nuevo_arbol, raiz);
              break;

          case 5:
              // Se muestra el grafico del árbol
              Graficar *g = new Graficar(raiz);
              g-> grafico();
              break;
        }
        system("clear");
    }
    while(op!=6);
}

// Función principal
int main(){
    // Se crea el arbol
    Arbol *nuevo_arbol = new Arbol();
    // Se crean los Nodos que contendran los datos ingresados
    Nodo *raiz = NULL;
    // Se llama al menú
    menu(nuevo_arbol, raiz);

    return 0;
}
